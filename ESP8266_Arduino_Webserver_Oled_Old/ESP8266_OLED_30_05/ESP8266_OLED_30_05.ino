//kulti_esp8266

#include <ESP8266WebServer.h> 
#include <EEPROM.h>
#include <Ticker.h>  			//Ticker Library
#include <FS.h>

#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"

Ticker blinker;

SSD1306Wire  display(0x3c, D3, D5);

const byte pin_led = 16;      //D0 NODE MCU LED
const byte pin_esp_led = 2;   //D4 ESP LED
const byte interruptPin = 5;  //D1
const byte adcPin = A0; //A0 for HV
const byte buttonPin = 4; //D2 PUSH BUTTON FOR MODE

//EEPROM DATA
String str_dt = "";
String str_bkg = "";
String str_indexNo=""; 
String str_cf=""; 
String str_bkgTime="";
String str_sampleTime="";
String str_eff  = "";  //YYYY-MM-DDThh-mm-ss...0-18...CfCfCf...19-21...BtBtBtBtBtBtBt...22-28...StStStStStStSt.....29-35...EfEfEf...36-38
                  //.......BgBgBgBgBgBgBg.... 39-45.....InInInInInInIn........46-52

//AnalyseDT
String str_hr = "00"; 
String str_min = "00"; 
String str_sec = "00"; 
String str_day, str_mon = "01";
String str_yr = "2019";

//AnalyseCF
unsigned char int_cf = 95;
float ka = 0.00;

//Analyse BkgT SampleT Eff
unsigned int int_bkgTime = 0;
unsigned int int_sampleTime = 0;
unsigned char int_eff = 0;

//handleRoot
unsigned int int_address = 0;

//startbkgstate
unsigned int int_bkg = 0;
unsigned char int_cm = 0; // 0 for bkg and 1 for sample.
String str_cm = "Single_Bkg";
unsigned char int_auto = 0; //0 single 1 auto
bool timeState = true;

//counts and time
unsigned int int_cs = 0;
unsigned int int_cr = 0;
String str_cr = "00"; 
String str_cs = "00";
String str_netRaw = "00";
unsigned int int_netRaw = 0; 

//Save State
float float_mem = 0.0;
String str_mem = "00";

//updateBKG
float mda_bq = 0.00;
float lld_cpm = 0.00;
float rb_cpm = 0.00;
float rg_cpm = 0.00;
String str_lld = "00"; 
String str_mda = "00"; 

String str_hv = "00";
unsigned int int_hv = 0;
String str_onOff = "OFF";


//String str_indexNo = "0000000";
unsigned char int_decision = 0;
String str_decision = "0";
unsigned int int_indexNo = 0;

//unsigned char timeSec = 1;
//unsigned int countSec = 0;
unsigned int timeLimit = 0;
unsigned char auto_wait = 0;


unsigned char int_sec = 0;
unsigned char int_min = 0;
unsigned char int_hr = 0;
unsigned char int_day = 1;
unsigned char int_mon = 1;
unsigned int int_yr = 2019;

unsigned char displayState = 0;
unsigned char int_mode = 0;
unsigned char changeModeFlag = 0;

const char* filename = "/govindamukherjee.txt";

const char *ssid = "Alscin01";
const char *password = "";

ESP8266WebServer server(80);

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
 * connected to this access point to see it.
 */

void analyseDT()
{
    // Length (with one extra character for the null terminator)
    unsigned char str_len = str_dt.length() + 1; 
  
    // Prepare the character array (the buffer) 
    char char_array[str_len];
  
    // Copy it over 
    str_dt.toCharArray(char_array, str_len);

    //char_array[19] = "\0";
    str_sec = "" + String(char_array[17]) + String(char_array[18]);
    int_sec = str_sec.toInt();
    //Serial.println(str_sec);
    
    str_min = "" + String(char_array[14]) + String(char_array[15]);
    int_min = str_min.toInt();
    //Serial.println(str_min);

    str_hr = "" + String(char_array[11]) + String(char_array[12]);
    int_hr = str_hr.toInt();
    //Serial.println(str_hr);
    
    str_day = "" + String(char_array[8]) + String(char_array[9]);
    int_day = str_day.toInt();
    //Serial.println(str_day);

    str_mon = "" + String(char_array[5]) + String(char_array[6]);
    int_mon = str_mon.toInt();
    //Serial.println(str_mon);
    
    str_yr = "" + String(char_array[0]) + String(char_array[1]) + String(char_array[2]) + String(char_array[3]);
    int_yr = str_yr.toInt();
    //Serial.println(str_yr);
}

void analyseCF()
{
  if(int_cf == 60)
  {
    ka = 0.26;
  }
  else if(int_cf = 65)
  {
    ka = 0.38;
  }
  else if(int_cf = 70)
  {
    ka = 0.52;
  }
  else if(int_cf = 75)
  {
    ka = 0.68;
  }
  else if(int_cf = 80)
  {
    ka = 0.84;
  }
  else if(int_cf = 85)
  {
    ka = 1.04;
  }
  else if(int_cf = 90)
  {
    ka = 1.282;
  }
  else if(int_cf = 95)
  {
    ka = 1.645;
  }
  else if(int_cf = 99)
  {
    ka = 2.327;
  }
  //Serial.println("ka: " + String(ka, 3));
}


void handleRoot() 
{

	//String IPaddress = WiFi.localIP().toString();	
  //Serial.println("ipaddress: " + IPaddress);
  //IPaddress = "192.168.1.15";
  
	
	String html ="<!DOCTYPE html> <html> <head> <meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1\" /> <style> *{ box-sizing: border-box; } body { font-family: Arial; padding: 10px; background: #f1f1f1; } .topnav { overflow: hidden; background-color: #333; } .topnav a { float: left; display: block; color: #f2f2f2; text-align: center; padding: 14px 16px; text-decoration: none; } .topnav a:hover { background-color: #ddd; color: black; } .leftcolumn { float: left; width: 75%; } .rightcolumn { float: left; width: 25%; background-color: #f1f1f1; padding-left: 20px; } .fakeimg { background-color: #aaa; width: 100%; padding: 20px; } .card { background-color: white; padding: 20px; margin-top: 20px; } .row:after { content: \"\"; display: table; clear: both; } .footer { padding: 20px; text-align: center; background: #ddd; margin-top: 20px; } textarea { width: 100%; height: 100%; -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */ -moz-box-sizing: border-box; /* Firefox, other Gecko */ box-sizing: border-box; /* Opera/IE 8+ */ } select { width: 100%; -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */ -moz-box-sizing: border-box; /* Firefox, other Gecko */ box-sizing: border-box; /* Opera/IE 8+ */ } input { width: 100%; -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */ -moz-box-sizing: border-box; /* Firefox, other Gecko */ box-sizing: border-box; /* Opera/IE 8+ */ } submit { width: 100%; -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */ -moz-box-sizing: border-box; /* Firefox, other Gecko */ box-sizing: border-box; /* Opera/IE 8+ */ } ";
  html +="@media screen and (max-width: 800px) { .leftcolumn, .rightcolumn { width: 100%; padding: 0; } } ";
  html +="@media screen and (max-width: 400px) { .topnav a { float: none; width: 100%; } } </style> </head> <body> <h1><center>ALSCIN MONITOR 001</center></h1> <div class=\"topnav\"> <a id = \"eff\", href=\"#\">getEff </a> <a id = \"bkg\", href=\"#\">getBkg </a> <a id = \"cf\", href=\"#\">getCF</a> <a id = \"bkgTime\", href=\"#\">getBkgT</a> <a id = \"sampleTime\", href=\"#\">getSampleT </a> <a id = \"lld\", href=\"#\">calLLD </a> <a id = \"mda\", href=\"#\">calMDA </a> <a id = \"hv\", href=\"#\">calHV </a> <a id = \"mem\", href=\"#\">calMem </a> <a id = \"help\", href=\"data.json\" style=\"float:right\">Help</a> </div> <div class=\"row\"> <div class=\"leftcolumn\"> <div class=\"card\"> <h2> <center> Alscin01: <button id=\"onOff\" style=\"border: 2px solid red; background-color: white; color: red; padding: 14px 28px; cursor: pointer; font-size: 20px; border-radius: 50px 50px 50px 50px;\" onclick=\"onOff()\">";
  html += str_onOff;
  html +="str_onOff</button> &nbsp&nbsp DateTime: <span id=\"dateTime\"> getDT </span> &nbsp&nbsp <br /><br /> <span id=\"countingMode\"> Mode </span> &nbsp&nbsp Gross: <a id=\"countRaw\", href=\"#\"> 00 </a> &nbsp&nbsp Net: <a id=\"netRaw\", href=\"#\"> 00 </a> &nbsp&nbsp Sec: <a id=\"countSec\", href=\"#\"> 00 </a> <br /> </center> </h2> <div id=\"updateView\"> <label for=\"fader\">Update rate in Milliseconds (Default 1000): </label> <output for=\"fader\" id=\"volume\">1000</output> <br /> <input type=\"range\" min=\"500\" max=\"10000\" value=\"1000\" id=\"fader\" step=\"1\" oninput=\"outputUpdate(value)\"> </div> <div class=\"fakeimg\" style=\"height:570px;\"><textarea id = \"txtArea\" cols=\"50\" rows=\"5\" style=\"overflow:scroll;\">SAVE </textarea> </div> <br /> Filename to Save As: &nbsp <input id=\"inputFileNameToSaveAs\" style=\"width:300px;\"></input> &nbsp &nbsp &nbsp &nbsp <button style=\"border: 2px solid brown; background-color: white; color: brown; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 50px 50px 50px;\" onclick=\"saveTextAsFile()\">Download</button> <button style=\"border: 2px solid darkgreen; background-color: white; color: darkgreen; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 50px 50px 50px;\" onclick=\"readSPIFFS()\">Read</button> <button style=\"border: 2px solid cyan; background-color: white; color: cyan; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 50px 50px 50px;\" onclick=\"clearTextArea()\">Clear</button> <button style=\"border: 2px solid darkmagenta; background-color: white; color: darkmagenta; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 50px 50px 50px;\" onclick=\"formatSPIFFS()\">Format</button> </div> </div> <div class=\"rightcolumn\"> <div class=\"card\"> <h3>Control Switches: </h3> <p> <input type=\"button\" value=\"Start Sample\" style=\"border: 2px solid green; background-color: white; color: green; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 10px 10px 50px 10px;\" onclick=\"startSamCounting()\"> <br /><br /> <input type=\"button\" value=\"Start Bkg\" style=\"border: 2px solid black; background-color: white; color: black; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 10px 50px 10px 50px;\" onclick=\"startBkgCounting()\"> <br /><br /> <input type=\"button\" value=\"Auto Start\" style=\"border: 2px solid orange; background-color: white; color: orange; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 10px 50px 10px;\" onclick=\"autoStart()\"> <br /><br /> <input type=\"button\" value=\"Stop\" style=\"border: 2px solid crimson; background-color: white; color: crimson; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 10px 50px 10px 50px;\" onclick=\"stopCounting()\"> <br /><br /> <input type=\"button\" value=\"Reset\" style=\"border: 2px solid purple; background-color: white; color: purple; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 10px 50px 10px;\" onclick=\"resetCounting()\"> <br /><br /> <input type=\"button\" value=\"Save\" style=\"border: 2px solid lime; background-color: white; color: lime; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 10px 10px 10px;\" onclick=\"saveCounting()\"> <br /><br /> </p> <h3>Parameter Settings: </h3> <p> <form action='http://";
  html += "192.168.1.15";
  html +="' method='POST'> DateTime: <input id=\"setDateTime\" type=\"datetime-local\" name=\"setDateTime\"> <br /> <br /> Confidence Level: <select id=\"setCF\" name=\"setCF\"> <option value=\"60\">60 %</option> <option value=\"65\">65 %</option> <option value=\"70\">70 %</option> <option value=\"75\">75 %</option> <option value=\"80\">80 %</option> <option value=\"85\">85 %</option> <option value=\"90\">90 %</option> <option value=\"95\">95 %</option> <option value=\"99\">99 %</option> </select> <br /> <br /> BackgroundTime (1 - 9999999 sec): <input id=\"setBkgT\" type=\"number\" name=\"setBkgT\" min=\"1\" max=\"9999999\"> <br /> <br /> SamplingTime (1 - 9999999 sec): <input id=\"setSampleT\" type=\"number\" name=\"setSampleT\" min=\"1\" max=\"9999999\"> <br /> <br /> Effeciency ( 1 - 100 %):<input id=\"setEff\" type=\"number\" name=\"setEff\" min=\"1\" max=\"100\"> <br /><br /> <input id=\"submitB\" style=\"border: 2px solid blue; background-color: white; color: blue; padding: 14px 28px; cursor: pointer; font-size: 10px; border-radius: 50px 50px 50px 50px;\" type=\"submit\"> </form> </p> </div> </div> </div> <div class=\"footer\"> <h2>RMS&DS -- RSSD -- BARC</h2> </div> <script> var changed = false; var indexNo = 0; var decision = 0; function loadDoc() { var xhttp = new XMLHttpRequest(); xhttp.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { var obj = JSON.parse(this.responseText); document.getElementById(\"onOff\").innerHTML = obj.data[0].onOff; document.getElementById(\"dateTime\").innerHTML = obj.data[1].dayDT + \"-\"+ obj.data[2].monDT + \"-\"+ obj.data[3].yrDT + \" &nbsp &nbsp \"+ obj.data[4].hrDT + \":\"+ obj.data[5].minDT + \":\"+ obj.data[6].secDT; document.getElementById(\"countingMode\").innerHTML = obj.data[7].countingMode; document.getElementById(\"countRaw\").innerHTML = obj.data[8].countRaw; document.getElementById(\"countSec\").innerHTML = obj.data[9].countSec; document.getElementById(\"eff\").innerHTML = \"Eff: \" + obj.data[10].eff + \" % &nbsp\"; document.getElementById(\"bkg\").innerHTML = \"Bkg: \" + obj.data[11].bkg + \" cnt &nbsp\"; document.getElementById(\"cf\").innerHTML = \"CF: \" + obj.data[12].cf + \" % &nbsp\"; document.getElementById(\"bkgTime\").innerHTML = \"BT: \" + obj.data[13].bkgTime + \" s &nbsp\"; document.getElementById(\"sampleTime\").innerHTML = \"ST: \" + obj.data[14].sampleTime + \" s &nbsp\"; document.getElementById(\"lld\").innerHTML = \"LLD: \" + obj.data[15].lld + \" cpm &nbsp\"; document.getElementById(\"mda\").innerHTML = \"MDA: \" + obj.data[16].mda + \" Bq &nbsp\"; document.getElementById(\"hv\").innerHTML = \"HV: \" + obj.data[17].hv + \" V &nbsp\"; document.getElementById(\"mem\").innerHTML = \"Mem: \" + obj.data[18].mem + \" % &nbsp\"; document.getElementById(\"netRaw\").innerHTML = obj.data[19].netRaw; decision = obj.data[20].decision; if(decision == 1) { decision = 0; saveCounting(); } } }; xhttp.open(\"GET\", \"data.json\", true); xhttp.send(); } var timedEvent = setInterval(function() { loadDoc(); }, 1000); function outputUpdate(vol) { clearInterval(timedEvent); timedEvent = setInterval(function() { loadDoc(); }, vol); document.querySelector('#volume').value = vol; delayVal = vol; } function clearTextArea() { console.log(\"clear button was clicked!\"); document.getElementById(\"txtArea\").innerHTML = \"CLEAR\"; } function readSPIFFS() { console.log(\"READ button was clicked!\"); document.getElementById(\"txtArea\").innerHTML = \"\"; var xhr = new XMLHttpRequest(); var url = \"readspiffs\"; var obj=document.getElementById(\"txtArea\"); xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { var txt=document.createTextNode(this.responseText); obj.appendChild(txt); obj.scrollTop = obj.scrollHeight; } }; xhr.open(\"GET\", url, true); xhr.send(); } function formatSPIFFS() { console.log(\"FORMAT button was clicked!\"); document.getElementById(\"txtArea\").innerHTML = \"\"; var xhr = new XMLHttpRequest(); var url = \"formatspiffs\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"txtArea\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function onOff() { var xhr = new XMLHttpRequest(); var url = \"onoff\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"onOff\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function startBkgCounting() { var xhr = new XMLHttpRequest(); var url = \"startbkgcounting\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"countingMode\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function startSamCounting() { var xhr = new XMLHttpRequest(); var url = \"startsamcounting\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"countingMode\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function autoStart() { var xhr = new XMLHttpRequest(); var url = \"autostart\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"countingMode\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function stopCounting() { var xhr = new XMLHttpRequest(); var url = \"stopcounting\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"countingMode\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function resetCounting() { var xhr = new XMLHttpRequest(); var url = \"resetcounting\"; xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { document.getElementById(\"countingMode\").innerHTML = this.responseText; } }; xhr.open(\"GET\", url, true); xhr.send(); } function saveCounting() { indexNo++; console.log(\"save button was clicked!\"); var xhr = new XMLHttpRequest(); var url = \"savecounting\"; var obj=document.getElementById(\"txtArea\"); xhr.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { var txt=document.createTextNode(this.responseText); obj.appendChild(txt); obj.scrollTop = obj.scrollHeight; } }; xhr.open(\"GET\", url, true); xhr.send(); } function destroyClickedElement(event) { document.body.removeChild(event.target); } function saveTextAsFile() { var textToSave = document.getElementById(\"txtArea\").value; var textToSaveAsBlob = new Blob([textToSave], {type:\"text/plain\"}); var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob); var fileNameToSaveAs = document.getElementById(\"inputFileNameToSaveAs\").value; var downloadLink = document.createElement(\"a\"); downloadLink.download = fileNameToSaveAs; downloadLink.innerHTML = \"Download File\"; downloadLink.href = textToSaveAsURL; downloadLink.onclick = destroyClickedElement; downloadLink.style.display = \"none\"; document.body.appendChild(downloadLink); downloadLink.click(); } </script> </body> </html>";

	server.send(200, "text/html", html);

  String Argument_Name, Clients_Response  = "";
	if (server.args() > 0 ) 
	{ 														// Arguments were received
    int_address = 0;
		for ( uint8_t i = 0; i < server.args(); i++ ) 
		{											
			Argument_Name = server.argName(i);
			Clients_Response = server.arg(i);
			
			if (Argument_Name == "setDateTime") 
			{
				Clients_Response = Clients_Response + ":00";
        
				str_dt = Clients_Response;
        analyseDT();
				for(int i=0;i<Clients_Response.length();i++) //loop upto string lenght www.length() returns length of string
				{
					EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
					int_address++;
				}
				EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));
			}
			else if (Argument_Name == "setCF") 
			{
				
				for(int i = Clients_Response.length(); i<3; i++)
				{
					Clients_Response = "0"+Clients_Response;
				}

        str_cf = Clients_Response;
        int_cf = str_cf.toInt();
        analyseCF();
				for(int i=0;i<Clients_Response.length();i++) //loop upto string lenght www.length() returns length of string
				{
					EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
					int_address++;
				}
				EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));
			}
			else if (Argument_Name == "setBkgT") 
			{
				
				for(int i = Clients_Response.length(); i<7; i++)
				{
					Clients_Response = "0"+Clients_Response;
				}

        str_bkgTime = Clients_Response;
        int_bkgTime = str_bkgTime.toInt();
        //analyseBkgTime();
				for(int i=0;i<Clients_Response.length();i++) //loop upto string lenght www.length() returns length of string
				{
					EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
					int_address++;
				}
				EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));
			}
			else if (Argument_Name == "setSampleT") 
			{
				for(int i = Clients_Response.length(); i<7; i++)
				{
					Clients_Response = "0"+Clients_Response;
				}

        str_sampleTime = Clients_Response;
        int_sampleTime = str_sampleTime.toInt();
        //analyseSampleTime();
				for(int i=0;i<Clients_Response.length();i++) //loop upto string lenght www.length() returns length of string
				{
					EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
					int_address++;
				}
				EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));
			}
			else if (Argument_Name == "setEff") 
			{				
				for(int i = Clients_Response.length(); i<3; i++)
				{
					Clients_Response = "0"+Clients_Response;
				}

        str_eff = Clients_Response;
        int_eff = str_eff.toInt();
        //analyseEff();
				for(int i=0;i<Clients_Response.length();i++) //loop upto string lenght www.length() returns length of string
				{
					EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
					int_address++;
				}
				EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));

        //Background Data 7 digit becomes 0 everytime you set date and time
        str_bkg = "0000000";
        int_bkg = 0;
        updateBkg();
        for(int i=0;i<str_bkg.length();i++) //loop upto string lenght www.length() returns length of string
        {
          EEPROM.write(int_address,Clients_Response[i]); //Write one by one with starting address of 0x0F
          int_address++;
        }
        EEPROM.commit();    //Store data to EEPROM
        //Serial.println("int_address: " + String(int_address, DEC));
			}
		}
    getResetState();    
	}
}

void getData() 
{   
	
	//This is a JSON formatted string that will be served. You can change the values to whatever like.
	// {"data":[{"dataValue":"1024"},{"dataValue":"23"}]} This is essentially what is will output you can add more if you like
	String text2 ="{ \"data\": [ { \"onOff\":\"";
	text2 += str_onOff;
	text2 +="\" }, { \"dayDT\":\"";
	text2 += str_day;
  text2 +="\" }, { \"monDT\":\"";
  text2 += str_mon;
	text2 +="\" }, { \"yrDT\":\"";
	text2 += str_yr;
	text2 +="\" }, { \"hrDT\":\"";
	text2 += str_hr;
	text2 +="\" }, { \"minDT\":\"";
	text2 += str_min;
	text2 +="\" }, { \"secDT\":\"";
	text2 += str_sec;
	text2 +="\" }, { \"countingMode\":\"";
	text2 += str_cm;
	text2 +="\" }, { \"countRaw\":\"";
	text2 += str_cr;
	text2 +="\" }, { \"countSec\":\"";
	text2 += str_cs;
  text2 +="\" }, { \"eff\":\"";
  text2 += str_eff;
  text2 +="\" }, { \"bkg\":\"";
  text2 += str_bkg;
  text2 +="\" }, { \"cf\":\"";
  text2 += str_cf;
  text2 +="\" }, { \"bkgTime\":\"";
  text2 += str_bkgTime;
  text2 +="\" }, { \"sampleTime\":\"";
  text2 += str_sampleTime;
  text2 +="\" }, { \"lld\":\"";
  text2 += str_lld;
  text2 +="\" }, { \"mda\":\"";
  text2 += str_mda;
  text2 +="\" }, { \"hv\":\"";
  text2 += str_hv;
  text2 +="\" }, { \"mem\":\"";
  text2 += str_mem;
  text2 +="\" }, { \"netRaw\":\"";
  text2 += str_netRaw;
  text2 +="\" }, { \"decision\":\"";
  text2 += str_decision;
	text2 +="\" } ] }";

	server.send(200, "text/html", text2);
}


void getOnOffState()
{
	digitalWrite(pin_esp_led,!digitalRead(pin_esp_led));
	str_onOff = digitalRead(pin_esp_led) ? "OFF" : "ON";
	server.send(200,"text/plain", str_onOff);
	//Serial.println("led toggle");
	//Serial.println(str_onOff);
}

void getStartBkgState()
{
  int_cm = 0;
  timeLimit = int_bkgTime;
  
  if(int_auto)
  {
    str_cm = " Auto_Bkg: ";
  }
  else
  {
    str_cm = " Single_Bkg: ";
  }

  if(int_cs >= timeLimit)
  {
    getResetState();
  }
  
  timeState = true;
  server.send(200,"text/plain", str_cm);
  
  //Serial.println("start BKG button");
}

void getStartSamState()
{
  int_cm = 1;
  timeLimit = int_sampleTime;

  if(int_auto)
  {
    str_cm = " Auto_Sample: ";
  }
  else
  {
    str_cm = " Single_Sample: ";
  }

  if(int_cs >= timeLimit)
  {
    getResetState();
  }
  
  timeState = true;
  server.send(200,"text/plain", str_cm);
  //Serial.println("start sample button");
}

void getAutoState()
{
  if(int_auto)
  {
    int_auto = 0;
    if(int_cm)
    {
      str_cm = " Single_Sample: ";
    }
    else
    {
      str_cm = " Single_Bkg: ";
    }
  }
  else
  {
    int_auto = 1;
    if(int_cm)
    {
      str_cm = " Auto_Sample: ";
    }
    else
    {
      str_cm = " Auto_Bkg: ";
    }
  }

  timeState = true;
  server.send(200,"text/plain", str_cm);  
}

void getStopState()
{
  timeState = false;
	server.send(200,"text/plain", str_cm);
}

void getResetState()
{
	int_cs = 0;
  str_cs = "0";
	int_cr = 0;
  str_cr = "0";
  rg_cpm = 0.00;
  int_netRaw = 0;
  str_netRaw = "00";
  server.send(200,"text/plain", "RESET");
}

void formatSPIFFS()
{
   //Format File System
    String strFormat = "FORMAT";
    if(SPIFFS.format())
    {
      strFormat = "File System Formated";
    }
    else
    {
      strFormat = "File System Formatting Error";
    }
    Serial.println(strFormat);
    int_indexNo = 0;
    str_indexNo = "0000000";
    server.send(200,"text/plain", strFormat);
}

void updateMemorySize()
{
  //Serial.print("Flash Real Size: ");
  //Serial.println(ESP.getFlashChipRealSize());
  //Serial.print("Flash Firmware Configured Size: ");
  //Serial.println(ESP.getFlashChipSize());
  
  FSInfo fsInfo;
  SPIFFS.info(fsInfo);
  Serial.print("FS Bytes: ");
  unsigned int int_usedBytes = fsInfo.usedBytes;
  unsigned int int_totalBytes = fsInfo.totalBytes;
  Serial.print(int_usedBytes);
  Serial.print(" / ");
  //Serial.println(int_totalBytes);

  float perBytes = ((int_usedBytes*100.0)/int_totalBytes);
  str_mem = String(perBytes, 1);
  Serial.println(str_mem);

  if(perBytes > 95.0)
  {
    formatSPIFFS();
  }
  
}

void readSPIFFS()
{
  
  //Read File data
  File f = SPIFFS.open(filename, "a+");
  String readData = "READ \n";
  if (!f) 
  {
    readData = readData + "file open failed";
    Serial.println(readData);
  }
  else
  {
      //Serial.println("Reading Data from File:");
      //Data from file
      for(int i=0;i<f.size();i++) //Read upto complete file size
      {
        readData = readData + (char)f.read();
        delay(1);
      }
      f.close();  //Close file
      //Serial.println("File Closed");
  }
  Serial.print(readData);
  server.send(200,"text/plain", readData);
}

void getSaveState()
{
  int_decision = 0;
  str_decision = "0";
    
  //str_indexNo = String(int_indexNo, DEC);
  for(unsigned char i = str_indexNo.length(); i<7; i++)
  {
    str_indexNo = "0"+str_indexNo;
  }
  
  String newData = "\n\nS.No.: " + str_indexNo + " DT: " + str_yr +"-"+str_mon+"-" + str_day +"T" +str_hr+":"+str_min+":"+ str_sec  + str_cm + " Gross: " + str_cr + " Net: " + str_netRaw + " T(s): " + str_cs + " \nCF(%): " + str_cf + " Bkg(cnts): " + str_bkg + " BkgT(s): " + str_bkgTime + " SampleT(s): " + str_sampleTime + " LLD(cnts): " + str_lld + " Eff(%): " + str_eff + " MDA(Bq): " + str_mda + " HV(V): " + str_hv + " Mem(%): " + str_mem;
  
  int_address = 46;
  for(unsigned char i=0;i<str_indexNo.length();i++) //loop upto string lenght www.length() returns length of string
  {
    EEPROM.write(int_address,str_indexNo[i]); //Write one by one with starting address of 0x0F
    int_address++;
  }
  EEPROM.commit();    //Store data to EEPROM
  //Serial.println("str_indexNo: " + str_indexNo);

  delay(10);
  
  File f = SPIFFS.open(filename, "a+");
  
  if (!f) 
  {
    Serial.println("file open failed");
  }
  else
  {
      f.println(newData);
      f.close();  //Close file
  }  
  server.send(200,"text/plain", newData);
  int_indexNo++;
  str_indexNo = String(int_indexNo, DEC);
  updateMemorySize();  
}

void updateBkg()
{
  //Background Data 7 digit becomes 0 everytime you set date and time
  for(unsigned char i = str_bkg.length(); i<7; i++)
  {
    str_bkg = "0"+str_bkg;
  }
  int_address = 39;
  for(int i=0;i<str_bkg.length();i++) //loop upto string lenght www.length() returns length of string
  {
    EEPROM.write(int_address,str_bkg[i]); //Write one by one with starting address of 0x0F
    int_address++;
  }
  EEPROM.commit();    //Store data to EEPROM
  //Serial.println("str_bkg: " + str_bkg);
  
  float sampleTime_cpm = int_sampleTime/60.0;
  float bkgTime_cpm = int_bkgTime/60.0;
  
  rb_cpm = int_bkg/bkgTime_cpm;
  
  float lc_cpm = 0.5*((sq(ka)/sampleTime_cpm) + ka*sqrt((sq(ka)/sq(sampleTime_cpm))+(4*rb_cpm*(bkgTime_cpm + sampleTime_cpm))/(sampleTime_cpm*bkgTime_cpm)));
  lld_cpm = lc_cpm + ka*sqrt((lc_cpm/sampleTime_cpm) + ((rb_cpm*(sampleTime_cpm + bkgTime_cpm))/(sampleTime_cpm*bkgTime_cpm)));

  str_lld = String(lld_cpm, 3);

  //Serial.println("lc_cpm: " + String(lc_cpm, 3));
  //Serial.println("lld_cpm: " + str_lld);
  
  mda_bq = lld_cpm/((int_eff/100.0)*60.0);
  
  str_mda = String(mda_bq, 3);
}

void changeState()
{
  int_hv = analogRead(adcPin) * 1.2;
  str_hv = String(int_hv, DEC);

  str_sec = String(int_sec, DEC);
  if(str_sec.length() < 2)
  {
    str_sec = "0" + str_sec;
  }

  int_sec++;   // 0 - 60 sec
  
  if((timeState) && (int_cs  < timeLimit))
  {
    int_cs = int_cs + 1;
    str_cs = String(int_cs, DEC);
    
    if(str_cs.length() < 2)
    {
      str_cs = "0" + str_cs;
    }

    if(int_cm)
    {
      float rgTime_cpm = (int_cs*1.0)/60.0;
      Serial.println("rgTime_cpm: " + String(rgTime_cpm, 4));
      
      rg_cpm = (int_cr*1.0)/rgTime_cpm;
      if(rg_cpm > rb_cpm)
      {
        //Serial.println("inside comparison");
        int_netRaw = ((rg_cpm - rb_cpm)*int_cs)/60;
        str_netRaw = String(int_netRaw, DEC);
      }
      Serial.println("str_netRaw: " + String(rb_cpm, 3) + " " + String(rg_cpm, 3) + " " +str_netRaw);
    }
    else
    {
      rg_cpm = 0.00;
      int_netRaw = 0;
      str_netRaw = "00";  
    }
    
    //Serial.println(str_cs);
    if(int_cs >= timeLimit)
    {
      if(!int_cm)
      {
        str_bkg = str_cr;
        int_bkg = int_cr;
        updateBkg();
      }
      int_decision = 1;
      str_decision = "1"; //decision = 1  means counting finished save it to file.
      auto_wait = 0;
    }
  }


  if((timeState) && (int_auto) && (int_cs >= timeLimit))
  {
    //getSaveState();
    auto_wait++;
    if(auto_wait > 2)
    {
      getResetState();
      auto_wait=0;
      //Serial.println("Auto Start Again");
    }
  }
  
  if(int_sec >= 60)
  {     
      int_sec = 0;
      int_min = int_min + 1;
      
      if(int_min >59)
      {
        int_min = 0;
        int_hr = int_hr + 1;
      }
      if(int_hr >23)
      {
        int_hr = 0;
        int_day = int_day + 1;
      }
      if((int_day >28) && (int_mon == 2))
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }
      else if((int_day >30) && (int_mon == 4))
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }
      else if((int_day >30) && (int_mon == 6))
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }
      else if((int_day >30) && (int_mon == 9))
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }
      else if((int_day >30) && (int_mon == 11))
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }
      else if(int_day >31)
      {
        int_day = 1;
        int_mon = int_mon + 1;
      }

      if(int_mon > 12)
      {
        int_mon = 1;
        int_yr = int_yr + 1;
      }

      str_min = String(int_min, DEC);
      if(str_min.length() < 2)
      {
        str_min = "0" + str_min;
      }

      str_hr = String(int_hr, DEC);
      if(str_hr.length() < 2)
      {
        str_hr = "0" + str_hr;
      }

      str_day = String(int_day, DEC);
      if(str_day.length() < 2)
      {
        str_day = "0" + str_day;
      }

      str_mon = String(int_mon, DEC);
      if(str_mon.length() < 2)
      {
        str_mon = "0" + str_mon;
      }

      str_yr = String(int_yr, DEC);
      
      str_dt = str_yr + "-" + str_mon + "-" + str_day + "T" + str_hr + ":" + str_min + ":" + str_sec;
      Serial.println(str_dt);  // 2019-04-24T22:59:00  i=0 to 18
  }

  displayState = 1;
  changeModeFlag = 1;
}

void handleButton() 
{
	//int reading = digitalRead(buttonPin);
  if(changeModeFlag)
  {
      changeModeFlag = 0;
      int_mode++;
      if(int_mode > 5)
      {
        int_mode = 0;
      }
  }
}

void handleInterrupt() 
{
  if((timeState) && (int_cs  < timeLimit))
  {
    int_cr++;
    str_cr = String(int_cr, DEC);
  }
}

/*
void drawFontFaceDemo() 
{
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "012345678901234567890"); //21
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "01234567890123"); //14
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "0123456789");  //10
}
*/

void drawTopLine()
{
  String topLine = "topLine";
  float float_bkgcps = (int_bkg*1.0)/(int_bkgTime*1.0);
  String str_bkgcps = String(float_bkgcps, 2);
  if(int_mode == 0)
  {
    topLine = "BKG e " + str_eff + "% b " + str_bkgcps + "cps";
    //topLine = "BKG e 100% b 99.99cps";
  }
  else if(int_mode == 1)
  {
    topLine = "GRS e " + str_eff + "% b " + str_bkgcps + "cps";
    //topLine = "GRS e 100% b 99.99cps";
  }
  else if(int_mode == 2)
  {
    topLine = "NET e " + str_eff + "% b " + str_bkgcps + "cps";
    //topLine = "NET e 100% b 99.99cps";
  }
  else if(int_mode == 3)
  {
    topLine = "RAT e " + str_eff + "% b " + str_bkgcps + "cps";
    //topLine = "RAT e 100% b 99.99cps";
  }
  else if(int_mode == 4)
  {
    topLine = "CNF cf " + str_cf + " %";
    //topLine = "CNF cf 100%";
  }
  else if(int_mode == 5)
  {
    topLine = "BAT hv " + str_hv + "V me " + str_mem + "%";
    //topLine = "BAT hv 1000V me 99.0%";
  }
  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  
  display.drawString(0, 0, topLine); //2132
  delay(10);
}

void drawMiddleLine()
{
  String middleLine = "middleLine";
  if(int_mode == 0)
  {
    if(!int_cm)
    {
      middleLine = "B " + str_cr;
    }
    else
    {
      middleLine = "B error";
    }
    //middleLine = "B 01234567";
  }
  else if(int_mode == 1)
  {
    if(int_cm)
    {
      middleLine = "G " + str_cr;
    }
    else
    {
      middleLine = "G error";
    }
    //middleLine = "G 01234567";
  }
  else if(int_mode == 2)
  {
    if(int_cm)
    {
      middleLine = "N " + str_netRaw;
    }
    else
    {
      middleLine = "N error";
    }
    //middleLine = "N 01234567";
  }
  else if(int_mode == 3)
  {
    if(int_cm)
    {
      float cpsSample = (int_cr * 1.0)/(int_cs * 1.0);
      middleLine = "s " + String(cpsSample, 2);
    }
    else
    {
      middleLine = "s error";
    }
    //middleLine = "s 01234567";
  }
  else if(int_mode == 4)
  {
    middleLine = "L " + str_lld + "cpm";
    //middleLine = "L 72.05cpm";
  }
  else if(int_mode == 5)
  {
    middleLine = str_yr +"-"+str_mon+"-" + str_day;
    //middleLine = "25-12-2019";
  }
  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_24);
  display.drawString(0, 14, middleLine); //2132
  delay(10);
}

void drawLastLine()
{
  String lastLine = "";
  if(int_mode == 0)
  {
    if(!int_cm)
    {
      lastLine = "t " + str_cs;
    }
    else
    {
      lastLine = "t error";
    }
    //lastLine = "t 01234567";
  }
  else if(int_mode == 1)
  {
    if(int_cm)
    {
      lastLine = "t " + str_cs;
    }
    else
    {
      lastLine = "t error";
    }
    //lastLine = "t 01234567";
  }
  else if(int_mode == 2)
  {
    if(int_cm)
    {
      lastLine = "t " + str_cs;
    }
    else
    {
      lastLine = "t error";
    }
    //lastLine = "t 01234567";
  }
  else if(int_mode == 3)
  {
    if(int_cm)
    {
      float cpmSample = (int_cr * 60.0)/(int_cs * 1.0);
      lastLine = "m " + String(cpmSample, 2);
    }
    else
    {
      lastLine = "m error";
    }
    //lastLine = "m 01234567";
  }
  else if(int_mode == 4)
  {
    lastLine = "M " + str_mda + "Bq";
    //lastLine = "M 22.729Bq";
  }
  else if(int_mode == 5)
  {
    lastLine = "t " + str_hr+":"+str_min+":"+ str_sec;
    //lastLine = "t 23:43:49";
  }
  
  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_24);
  display.drawString(0, 36, lastLine); //2132
  delay(10);
}

void setup() 
{
	pinMode(pin_led, OUTPUT);
	pinMode(pin_esp_led, OUTPUT);
	pinMode(interruptPin, INPUT_PULLUP);
  pinMode(buttonPin, INPUT_PULLUP);
  
	Serial.begin(115200);
  delay(10);

  //display.init();
  //display.flipScreenVertically();
  //display.setFont(ArialMT_Plain_10);
  delay(10);
  
  str_onOff = digitalRead(pin_led) ? "OFF" : "ON";
  
	Serial.print("Configuring access point...");
	/* You can remove the password parameter if you want the AP to be open. */
  IPAddress Ip(192, 168, 1, 15);
  IPAddress NMask(255, 255, 255, 0);
  WiFi.softAPConfig(Ip, Ip, NMask);
  WiFi.softAP(ssid, password);
	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.on("/", handleRoot);
  delay(10);
	server.on("/data.json", getData);
  delay(10);
	server.on("/onoff",getOnOffState);
  delay(10);  
	//server.on("/startbkgcounting",getStartBkgState);
  delay(10);
  //server.on("/startsamcounting",getStartSamState);
  delay(10);
	server.on("/stopcounting",getStopState);
  delay(10);
	server.on("/resetcounting",getResetState);
  delay(10);
	server.on("/savecounting",getSaveState);
  delay(10);
  server.on("/formatspiffs",formatSPIFFS);
  delay(10);
  server.on("/autostart",getAutoState);
  delay(10);
  server.on("/readspiffs",readSPIFFS);
  delay(10);  
	server.begin();
	delay(10);

  EEPROM.begin(512);
  delay(10);
  
	for(int i=0; i < 53; i++)
	{
		//Serial.println( String(i, DEC) + " : " + char(EEPROM.read(i)));
		if(i<19)
		{
			str_dt = str_dt + char(EEPROM.read(i));
			//Serial.println("Get DT: " + str_dt);
      //analyseDT();
		}
		else if(i<22)
		{
			str_cf = str_cf + char(EEPROM.read(i));
			//Serial.println("Get CF: " + str_cf);
		}
		else if(i<29)
		{
			str_bkgTime = str_bkgTime + char(EEPROM.read(i));
			//Serial.println("Get BkgT: " + str_bkgTime);
		}
		else if(i<36)
		{
			str_sampleTime = str_sampleTime + char(EEPROM.read(i));
			//Serial.println("Get Sample Time: " + str_sampleTime);
		}
		else if(i<39)
		{
			str_eff = str_eff + char(EEPROM.read(i));
			//Serial.println("Get Eff: " + str_eff);
		}
    else if(i<46)
    {
      str_bkg = str_bkg + char(EEPROM.read(i));      
      //Serial.println("Get Bkg: " + str_bkg);
    }
    else if(i<53)
    {
      str_indexNo = str_indexNo + char(EEPROM.read(i));
      //Serial.println("Get Index No. : " + str_indexNo);
    }
	}

  int_cf = str_cf.toInt();
  int_bkgTime = str_bkgTime.toInt();
  int_sampleTime = str_sampleTime.toInt();
  int_eff = str_eff.toInt();
  int_bkg = str_bkg.toInt();
  int_indexNo = str_indexNo.toInt();

  int_indexNo++;
  str_indexNo = String(int_indexNo, DEC);

  analyseDT();
  delay(10);
  analyseCF();
  delay(10);
  updateBkg();
  delay(10);
  updateMemorySize();
  delay(10);

  //Initialize File System
  
  if(SPIFFS.begin())
  {
    Serial.println("SPIFFS Initialize....ok");
  }
  else
  {
    Serial.println("SPIFFS Initialization...failed");
  }

  //Read File data
  File f = SPIFFS.open(filename, "a+");
  
  if (!f) 
  {
    Serial.println("file open failed");
  }
  else
  {
      //Data from file
      for(int i=0;i<f.size();i++) //Read upto complete file size
      {
        Serial.print((char)f.read());
        delay(1);
      }
      f.close();  //Close file
  }

  delay(10);
	attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, FALLING);
  delay(10);
  attachInterrupt(digitalPinToInterrupt(buttonPin), handleButton, FALLING);

	//Initialize Ticker every 0.5s
	blinker.attach(1.0, changeState); //Use <strong>attach_ms</strong> if you need time in ms
  delay(10);
}

void loop() 
{
	server.handleClient();
  if(displayState)
  {
     displayState = 0;
     delay(10);
     //display.clear();
     delay(10);
     //drawTopLine();
     //drawMiddleLine();
     //drawLastLine();
     delay(10);
     //display.display();
     delay(10);
  }
}
