
import os
import sys
import traceback
import logging

class Custom_CRC:

    def __init__(self, master):
        None

    def make_CRC(BitString):
        successBool = False
        try:
            print("\n\nStart Analyzing ...\n")
            
            Res[8] = ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a']
            CRC[7] = [0, 0, 0, 0, 0, 0, 0]
            i = 0
            DoInvert = 'a';
   
            for (i=0; i<strlen(BitString); ++i)
              {
              DoInvert = ('1'==BitString[i]) ^ CRC[6];         // XOR required?

              CRC[6] = CRC[5] ^ DoInvert;
              CRC[5] = CRC[4];
              CRC[4] = CRC[3];
              CRC[3] = CRC[2] ^ DoInvert;
              CRC[2] = CRC[1] ^ DoInvert;
              CRC[1] = CRC[0];
              CRC[0] = DoInvert;
              } 
      
   for (i=0; i<7; ++i)  Res[6-i] = CRC[i] ? '1' : '0'; // Convert binary to ASCII
   Res[7] = 0;                                         // Set string terminator

   return(Res);
            successBool = True
               
        except Exception as err:
            logging.error(traceback.format_exc())
            successBool = False

        finally:
            if (successBool):
                print("\n... Data Analyzing Successful Analyzed.txt \n")
            else:
                print("\n... Data Analyzing Unsuccessful (Try Again)\n")

            return successBool
                    
def main():
    var = Custom_CRC
    result = var.make_CRC()    
    

if __name__ == "__main__":
    main()


