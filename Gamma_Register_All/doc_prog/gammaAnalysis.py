import time
from time import sleep
import traceback
import logging
import datetime
import gammaCRC

import os
import tkinter
from tkinter import messagebox
from tkinter.ttk import Combobox
from tkinter.filedialog import *
import math


class do_Analysis:

    

    def __init__(self, master):
        None


    def startAnalysis():
        try:
            print("\n\nStart Analyzing ...\n")
            successBool = False
            dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')
            oldDateTime = dateTimeData
            presetDelay = 0
            fDialog = tkinter.filedialog.askopenfilename(title="Please select a [xyz]_HRM.txt file")
            

            if os.path.exists(fDialog):
                try:
                    print("\nRead file from: %s ............\n" % fDialog)
                    f0 = open(fDialog, "r+")
                    f1 = open(fDialog[:-4] + "_Analyzed.txt", "w+")
                    var = f0.read()
                    #print(len(var))
                    i = 0
                    outputStr = ""
                    #dateTime = ["01","01","01","01","01"]
                    dateTimeCount = 0
                    dataCount = 0
                    dataValue = [0, 0, 0, 0, 0]
                    dataIndex = 0
                    crc8Calculator = gammaCRC.crc_Calculate
                    doseValue = 0
                    presetDelay = 0

                    
                    
                    while(i < (len(var) - 1)):
                        strTwo = var[i] + var[i+1]
                        intTwo = int(strTwo, 16)
                        strPrint01 = "i:" + str(i) + " strTwo:" + strTwo + " intTwo:" + str(intTwo) + " hexTwo:" + hex(intTwo)  + "\n"
                        print(strPrint01)

                        dataValue[dataIndex] = intTwo
                        dataIndex = dataIndex + 1

                        oneMinStart = 0

                        if(dataIndex >= 5):
                            print(dataValue)
                            if((dataValue[0] == 0xff) & (dataValue[1] == 0xfe) & (dataValue[2] == 0xfd) & (dataValue[3] == 0xfc)):
                                dataIndex = 0
                                if(dataValue[4] == 0xF6):
                                    print("Data Reading Starts")
                                elif(dataValue[4] == 0xfa):
                                    print("Data Reading Ends")
                                elif(dataValue[4] == 0xf1):
                                    print("one minute start")
                                    oneMinStart = 1
                                    print(oneMinStart)
                                elif(dataValue[4] == 0x01):
                                    presetDelay = 1
                                    print("time = 1 min")
                                elif(dataValue[4] == 0x0a):
                                    presetDelay = 10
                                    print("time = 10 min")
                                elif(dataValue[4] == 0x1e):
                                    presetDelay = 30
                                    print("time = 30 min")

                                if((i+21) <= (len(var)-1)):                                    
                                    strDD01 = var[i+2] + var[i+3]
                                    intDD01 = int(strDD01, 16)                               
                                    strDD02 = var[i+4] + var[i+5]
                                    intDD02 = int(strDD02, 16)
                                    strMM01 = var[i+6] + var[i+7]
                                    intMM01 = int(strMM01, 16)
                                    strMM02 = var[i+8] + var[i+9]
                                    intMM02 = int(strMM02, 16)
                                    strYY01 = var[i+10] + var[i+11]
                                    intYY01 = int(strYY01, 16)
                                    strYY02 = var[i+12] + var[i+13]
                                    intYY02 = int(strYY02, 16)
                                    strhh01 = var[i+14] + var[i+15]
                                    inthh01 = int(strhh01, 16)
                                    strhh02 = var[i+16] + var[i+17]
                                    inthh02 = int(strhh02, 16)
                                    strmm01 = var[i+18] + var[i+19]
                                    intmm01 = int(strmm01, 16)
                                    strmm02 = var[i+20] + var[i+21]
                                    intmm02 = int(strmm02, 16)
                                     
                                    strDD = chr(intDD01) + chr(intDD02)
                                    strMM = chr(intMM01) + chr(intMM02)
                                    strYY = chr(intYY01) + chr(intYY02)
                                    strhh = chr(inthh01) + chr(inthh02)
                                    strmm = chr(intmm01) + chr(intmm02)
                                    dateTimeFormat = strDD + "-" + strMM + "-" + strYY + "-" + strhh + "-" + strmm
                                    print(strDD01 + strDD02 + strMM01 + strMM02 + strYY01 + strYY02 + strhh01 + strhh02 + strmm01 + strmm02)
                                    try:
                                        dateTimeData = datetime.datetime.strptime(dateTimeFormat, '%d-%m-%y-%H-%M')
                                        if(oneMinStart == 1):
                                            oneMinStart = 0     #one minute correction. When one minute set to GR, timestamp saved in memory, timer = 0 and data come after one min. but software consider memory saved timestamp for 1st data.
                                            #print(dateTimeData)
                                            dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                            #print(dateTimeData)
                                    except Exception as err:
                                        logging.error(traceback.format_exc())
                                        dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')
                                        
                                    i=i+20
                            else:
                                msg = [dataValue[0], dataValue[1], dataValue[2], dataValue[3]]
                                crcResult = crc8Calculator.crc8(msg)
                                if(crcResult == dataValue[4]):
                                    doseValue = (dataValue[0] * math.pow(16,3)) + (dataValue[1] * math.pow(16,2)) + (dataValue[2] * math.pow(16,1)) + (dataValue[3] * math.pow(16,0))
                                    int_data = doseValue
                                    #print(doseValue)
                                    #print(crcResult)
                                    print("crc matched")                                    
                                    outputStr = "Date-Time: " + str(dateTimeData) + " Delay: " + str(presetDelay) + " min Counts: " + str(int_data) + " cpm Field: " + str(round(int_data*0.833, 2)) + " uR/h "
                                    f1.write(outputStr)
                                    f1.write("\n")
                                    #print(outputStr + "\n")
                                    dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                    dataIndex = 0
                                    presetDelay = 1
                                else:
                                    dataValue[0] = dataValue[1]
                                    dataValue[1] = dataValue[2]
                                    dataValue[2] = dataValue[3]
                                    dataValue[3] = dataValue[4]
                                    dataIndex = 4

                        """
                        if(intTwo == 255):
                            if(dateTimeCount == 0):
                                dateTimeCount = dateTimeCount + 1
                            else:
                                dateTimeCount = 0
                                dateTimeCount = dateTimeCount + 1
                        elif(intTwo == 254):
                            if(dateTimeCount == 1):
                                dateTimeCount = dateTimeCount + 1
                            else:
                                dateTimeCount = 0
                        elif(intTwo == 253):
                            if(dateTimeCount == 2):
                                dateTimeCount = dateTimeCount + 1
                            else:
                                dateTimeCount = 0
                        elif(intTwo == 252):
                            if(dateTimeCount == 3):
                                dateTimeCount = dateTimeCount + 1
                            else:
                                dateTimeCount = 0


                        if(dateTimeCount == 4):
                            dataIndex = 0
                            if((i + 21) < len(var)):
                                strDD01 = var[i+2] + var[i+3]
                                intDD01 = int(strDD01, 16)                               
                                strDD02 = var[i+4] + var[i+5]
                                intDD02 = int(strDD02, 16)
                                strMM01 = var[i+6] + var[i+7]
                                intMM01 = int(strMM01, 16)
                                strMM02 = var[i+8] + var[i+9]
                                intMM02 = int(strMM02, 16)
                                strYY01 = var[i+10] + var[i+11]
                                intYY01 = int(strYY01, 16)
                                strYY02 = var[i+12] + var[i+13]
                                intYY02 = int(strYY02, 16)
                                strhh01 = var[i+14] + var[i+15]
                                inthh01 = int(strhh01, 16)
                                strhh02 = var[i+16] + var[i+17]
                                inthh02 = int(strhh02, 16)
                                strmm01 = var[i+18] + var[i+19]
                                intmm01 = int(strmm01, 16)
                                strmm02 = var[i+20] + var[i+21]
                                intmm02 = int(strmm02, 16)
                                 
                                strDD = chr(intDD01) + chr(intDD02)
                                strMM = chr(intMM01) + chr(intMM02)
                                strYY = chr(intYY01) + chr(intYY02)
                                strhh = chr(inthh01) + chr(inthh02)
                                strmm = chr(intmm01) + chr(intmm02)
                                dateTimeFormat = strDD + "-" + strMM + "-" + strYY + "-" + strhh + "-" + strmm
                                try:
                                    dateTimeData = datetime.datetime.strptime(dateTimeFormat, '%d-%m-%y-%H-%M')
                                except Exception as err:
                                    logging.error(traceback.format_exc())
                                    dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')

            
                               
                                strPrint02 = "DATE: " + strDD01 + strDD02 + "-" + str(intDD01) + str(intDD02) + "-" + chr(intDD01) +  chr(intDD02) + "\n"
                                strPrint03 = "MONTH: " + strMM01 + strMM02 + "-" + str(intMM01) + str(intMM02) + "-" + chr(intMM01) +  chr(intMM02) + "\n"
                                strPrint04 = "YEAR: " + strYY01 + strYY02 + "-" + str(intYY01) + str(intYY02) + "-" + chr(intYY01) +  chr(intYY02) + "\n"
                                strPrint05 = "HOUR: " + strhh01 + strhh02 + "-" + str(inthh01) + str(inthh02) + "-" + chr(inthh01) +  chr(inthh02) + "\n"
                                strPrint06 = "MINUTE: " + strmm01 + strmm02 + "-" + str(intmm01) + str(intmm02) + "-" + chr(intmm01) +  chr(intmm02) + "\n"
                                
                                print(dateTimeFormat + " : " + str(dateTimeData) + " : " + strPrint02 + strPrint03 + strPrint04 + strPrint05 + strPrint06)
                                #f1.write(dateTimeFormat + " : " + str(dateTimeData) + " : " + strPrint02 + strPrint03 + strPrint04 + strPrint05 + strPrint06)
                               
                                i = i + (21-1)
                                
                                
                            dateTimeCount = 0
                        
                        netDateTime = (dateTimeData - oldDateTime).total_seconds()/60.0
                    
                        #print(str(oldDateTime) + "old date \n")
                        #print(str(dateTimeData) + "date time data\n")
                        #print(str(netDateTime) + "net date \n")
                        oldDateTime = dateTimeData

                        if(netDateTime > 0):
                            if(netDateTime < 1576801):
                                if(netDateTime < 6):
                                    presetDelay = 1
                                elif(netDateTime < 25):
                                    presetDelay = 10
                                elif(netDateTime < 65):
                                    presetDelay = 30
                                else:
                                    presetDelay = netDateTime
                        

                        #print(dataIndex)
                        dataValue[dataIndex] = intTwo
                        #dataIndex = dataIndex + 1
                        if(dataIndex >=4):
                            print(dataValue)
                            msg = [dataValue[0], dataValue[1], dataValue[2], dataValue[3]]
                            crcResult = crc8Calculator.crc8(msg)
                            if(crcResult == dataValue[4]):
                                doseValue = (dataValue[0] * math.pow(16,3)) + (dataValue[1] * math.pow(16,2)) + (dataValue[2] * math.pow(16,1)) + (dataValue[3] * math.pow(16,0))
                                int_data = doseValue
                                #print(doseValue)
                                #print(crcResult)
                                print("crc matched")
                                outputStr = "Date-Time: " + str(dateTimeData) + " Delay: " + str(presetDelay) + " min Counts: " + str(int_data) + " cpm Field: " + str(round(int_data*0.833, 2)) + " uR/h "
                                f1.write(outputStr)
                                f1.write("\n")
                                #print(outputStr + "\n")
                                dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                dataIndex = 0
                            else:
                                dataValue[0] = dataValue[1]
                                dataValue[1] = dataValue[2]
                                dataValue[2] = dataValue[3]
                                dataValue[3] = dataValue[4]
                                dataIndex = 4
                        else:
                            dataIndex = dataIndex + 1"""
                                
                        
                            
                        i = i+2 

                        
 
                    f0.close()
                    f1.close()

                    #for i in var:
                        

                    """
                    var = f0.read().replace('\n', '')
                    sep = var.split("\\n\\r")
                    f0.close()

                    oldDateTime = dateTimeData
                    presetDelay = 0
                    for i in sep:
                        
                        if(len(i) > 0):
                            print(i)
                            pattern = re.compile(r'[0-9]{10,10}', re.I | re.S | re.M)
                            result = pattern.search(i)
                            print(result)
                            if(result):
                                successBool = True
                                try:
                                    dateTimeData = datetime.datetime.strptime(result.group(), '%d%m%y%H%M')
                                except Exception as err:
                                    logging.error(traceback.format_exc())
                                    dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')

                            netDateTime = (dateTimeData - oldDateTime).total_seconds()/60.0
                            
                            print(str(oldDateTime) + "\n")
                            print(str(dateTimeData) + "\n")
                            print(str(netDateTime) + "\n")
                            oldDateTime = dateTimeData

                            
                            if(netDateTime > 0):
                                if(netDateTime < 1576801):
                                    if(netDateTime < 6):
                                        presetDelay = 1
                                    elif(netDateTime < 25):
                                        presetDelay = 10
                                    elif(netDateTime < 65):
                                        presetDelay = 30
                                    else:
                                        presetDelay = netDateTime
                            
                            #pattern = re.compile(r'\\x[a-fA-F0-9][a-fA-F0-9]\\x[a-fA-F0-9][a-fA-F0-9]', re.I | re.S | re.M)
                            #result = pattern.search(i)
                            
                            #if(result):
                                #successBool = True
                            pattern = re.compile(r'\\x[a-fA-F0-9][a-fA-F0-9]\\x[a-fA-F0-9][a-fA-F0-9]\\x[a-fA-F0-9][a-fA-F0-9]\\x[a-fA-F0-9][a-fA-F0-9]')
                            result = pattern.match(i)
                            print("result")
                            print(result)
                            if(result):
                                successBool = True
                                int_data = int(result.group().replace("\\x", ''), 16)
                                if(int_data > 0):
                                    outputStr = "Date-Time: " + str(dateTimeData) + " Delay: " + str(presetDelay) + " min Counts: " + str(int_data) + " cpm Field: " + str(round(int_data*0.833, 2)) + " uR/h "
                                    f1.write(outputStr)
                                    f1.write("\n")
                                    print(outputStr + "\n")
                                dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                            else:
                                dateTimeData = dateTimeData + datetime.timedelta(minutes=1)

                    f1.close() """
                finally:
                    if (f0.closed):
                        print("f0 is closed")
                    else:
                        f0.close()

                    if (f1.closed):
                        print("f1 is closed")
                    else:
                        f1.close()
            else:
                successBool = False
               
        except Exception as err:
            logging.error(traceback.format_exc())
            successBool = False

        finally:
            if (successBool):
                print("\n... Data Analyzing Successful %s_Analyzed.txt \n" % fDialog[:-4])
            else:
                print("\n... Data Analyzing Unsuccessful (Try Again)\n")

            return successBool
                    
def main():
    var = do_Analysis
    result = var.startAnalysis()    
    

if __name__ == "__main__":
    main()


