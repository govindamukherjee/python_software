#Date: 11-03-2019

import os
import tkinter
from tkinter import messagebox
from tkinter.ttk import Combobox
from tkinter.filedialog import *

import datetime
import traceback
import logging
import threading
import xlsxwriter

import gammaSerial
from gammaSerial import *

import gammaCOM
from gammaCOM import *

import gammaCRC
from gammaCRC import *

import math
from math import *


class GR_layer01:

    def __init__(self, master):
        self.master = master
        self.master.title("Gamma Register Software V2")
        self.master.geometry("1080x600")

        self.frame = Frame(self.master)
        Grid.rowconfigure(self.master, 0, weight=1)
        Grid.columnconfigure(self.master, 0, weight=1)
        self.frame.grid(row=0, column=0, sticky=N+S+E+W)

        self.textAreaText = ""
        self.getRawDataFlag = 0
        self.getRawDataText = ""
        self.readFlag = False

        self.menuGUI()
        self.outputGUI()
        self.con = ComConnect()
        self.autoConnect()

    def showHOWTOManual(self):
        manualFile = "GammaRegisterSoftwareManual02.pdf"
            
        if(sys.platform.startswith('darwin')):
            os.system(manualFile)
        elif sys.platform.startswith('linux'):
            os.system(manualFile)
        elif sys.platform.startswith('win32'):
            os.startfile(manualFile)

    def showTerminalManual(self):
        manualFile = "GammaRegisterTerminalManual01.pdf"
            
        if(sys.platform.startswith('darwin')):
            os.system(manualFile)
        elif sys.platform.startswith('linux'):
            os.system(manualFile)
        elif sys.platform.startswith('win32'):
            os.startfile(manualFile)

    def showTerminal(self):
        manualFile = "Terminal1.exe"
            
        if(sys.platform.startswith('darwin')):
            os.system(manualFile)
        elif sys.platform.startswith('linux'):
            os.system(manualFile)
        elif sys.platform.startswith('win32'):
            os.startfile(manualFile)
        
    def menuGUI(self):
        self.menubar = Menu(self.master)
        self.master.config(menu=self.menubar)
        
        self.directMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='Direct Access', menu=self.directMenu)
        self.directMenu.add_command(label='Terminal', command=self.showTerminal)

        self.softwareMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='Help', menu=self.softwareMenu)
        self.softwareMenu.add_command(label='HOW TO Software Manual', command=self.showHOWTOManual)
        self.softwareMenu.add_command(label='Terminal Manual', command=self.showTerminalManual)

        
    def printTextArea(self, printData):                    #to insert data into the end of multi-line text widget
        self.txtArea.config(state=NORMAL)
        self.txtArea.insert(END, printData)
        self.txtArea.see("end")
        self.txtArea.update()
        self.txtArea.config(state=DISABLED)

    def continuousRead(self):                               # thread access this method continuously to update the text widget
        self.textAreaText = ""
        self.autoConnectText = 0
        patternRawStart = re.compile("fffefdfcf6", re.S | re.M)                 #Regular Expression How To .pdf
        patternRawEnd = re.compile("fffefdfcfa", re.S | re.M)
        patternReadingFreq1A = re.compile("fffefdfcf1", re.S | re.M)
        patternReadingFreq2B = re.compile("fffefdfcf2", re.S | re.M)
        patternReadingFreq3C = re.compile("fffefdfcf3", re.S | re.M)
        patternDateTimeStart = re.compile("fffefdfcf4", re.S | re.M)
        patternDateTimeEnd = re.compile("fffefdfcf8", re.S | re.M)
        patternDeleteTillNowStart = re.compile("fffefdfcf7", re.S | re.M)
        patternDeleteTillNowEnd = re.compile("fffefdfcfb", re.S | re.M)
        patternDeleteAllStart = re.compile("fffefdfcf5", re.S | re.M)
        patternDeleteAllEnd = re.compile("fffefdfcf9", re.S | re.M)

        while(self.readFlag):                                                               #stop the while loop from running when not connected to any com port using readFlag
            resultByte = self.con.readSerial()
            if(resultByte != None):
                #print(type(resultByte))
                #readResult1 = result.replace("'", "")
                #readResult = readResult1.replace(" ", "")
                self.textAreaText = str(resultByte.hex())
                
                result = patternRawStart.search(self.textAreaText)
                if(result):
                        self.printTextArea("\n Raw Data Start Receiving ...\n")              
                
                
                result = patternDateTimeStart.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Date Time Start Setting...\n")
                    self.normalDisable(0)

                result = patternDeleteTillNowStart.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Delete Data Till Now Starts...\n")
                    self.normalDisable(1)

                result = patternDeleteAllStart.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Delete Data All Starts...\n")
                    self.normalDisable(1)

                self.printTextArea(self.textAreaText)    
                print(self.textAreaText)
                print("\n\n")

                result = patternReadingFreq1A.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Acquisition Time set to 1 min Successfully\n")
                    self.autoConnectText = 1

                result = patternReadingFreq2B.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Acquisition Time set to 10 min Successfully\n")

                result = patternReadingFreq3C.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Acquisition Time set to 30 min Successfully\n")


                result = patternRawEnd.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Raw Data Received Successfully\n")

                result = patternDateTimeEnd.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Date Time Set Successfully\n")
                    self.normalDisable(1)

                result = patternDeleteTillNowEnd.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Delete Data Till Now End Successfully\n")
                    self.normalDisable(1)

                result = patternDeleteAllEnd.search(self.textAreaText)
                if(result):
                    self.printTextArea("\n Delete Data All End Successfully\n")
                    self.normalDisable(1)
                    

                
                #self.textAreaText = self.textAreaText + readResult
                #self.textAreaText = self.textAreaText + result
                #mod01 = result.replace(" ", "")
                #self.textAreaText = self.textAreaText + str(result)
                
                #if(len(self.textAreaText) > 0):
                    
                if(self.getRawDataFlag > 0):
                    #self.getRawDataText = self.getRawDataText + self.textAreaText
                    self.getRawDataText = self.getRawDataText + resultByte.hex()
                    """
                    testString = resultByte.decode('ascii')
                    result = patternRawStart.search(testString)
                    if(result):
                        self.printTextArea("\n Raw Data Start Receiving ...\n")

                    result = patternRawEnd.search(testString)
                    if(result):
                        self.printTextArea("\n Raw Data Received Successfully\n")

                else:
                    self.textAreaText = self.textAreaText + resultByte.decode('ascii')
                    result = patternReadingFreq1A.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Acquisition Time set to 1 min Successfully\n")
                        self.autoConnectText = 1

                    result = patternReadingFreq2B.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Acquisition Time set to 10 min Successfully\n")

                    result = patternReadingFreq3C.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Acquisition Time set to 30 min Successfully\n")

                    result = patternDateTime.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Date Time Set Successfully\n")

                    result = patternDeleteTillNow.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Delete Data Till Now Successfully\n")
                        self.normalDisable(1)

                    result = patternDeleteAll.search(self.textAreaText)
                    if(result):
                        self.printTextArea("\n Delete Data All Successfully\n")
                        self.normalDisable(1)"""
    
                self.textAreaText = ""

            
            
    def normalDisable(self, ndFlag):
        if(ndFlag == 0):
            self.butAutoRearm.config(state=DISABLED)
            self.butConnect.config(state=DISABLED)
            self.butExcelExport.config(state=DISABLED)
            self.butAnalyze.config(state=DISABLED)
            self.butGetRawData.config(state=DISABLED)
            self.butSetRecordDelay.config(state=DISABLED)
            self.cbox01.config(state=DISABLED)
            self.e1.config(state=DISABLED)
            self.butGetDateTime.config(state=DISABLED)
            self.butSetDateTime.config(state=DISABLED)
            self.butDeleteRecordData.config(state=DISABLED)
            #self.butClearScreen.config(state=DISABLED)
            if(self.getRawDataFlag == 1):
                self.butSaveRawData.config(state=DISABLED)
            self.getRawDataFlag = 0
        else:
            self.butAutoRearm.config(state=NORMAL)
            self.butConnect.config(state=NORMAL)
            self.butExcelExport.config(state=NORMAL)
            self.butAnalyze.config(state=NORMAL)
            self.butGetRawData.config(state=NORMAL)
            self.butSetRecordDelay.config(state=NORMAL)
            #self.cbox01.config(state=NORMAL)
            self.e1.config(state=NORMAL)
            self.butGetDateTime.config(state=NORMAL)
            self.butSetDateTime.config(state=NORMAL)
            self.butDeleteRecordData.config(state=NORMAL)
            #self.butClearScreen.config(state=NORMAL)
            if(self.getRawDataFlag == 1):
                self.butSaveRawData.config(state=NORMAL)

    
        
    def connectCOM(self):
        
        self.normalDisable(0)
        try:
            if(self.con.scr.isOpen()):
                self.printTextArea("\n\n... Disconnecting from " + self.cbox01.get() + "...\n")
            else:
                self.printTextArea("\n\n... Connecting to " + self.cbox01.get() + "...\n")

            if(self.con.scr.isOpen()):
                result = self.con.disconnectSerial()
                if(result):
                    self.readFlag = False
                    self.printTextArea("\nDisconnected Successfully \n")
                    self.cbox01.config(state=NORMAL)
                    self.butConnect.config(state=NORMAL)
                    self.butAutoRearm.config(state=NORMAL)
                    self.butExcelExport.config(state=NORMAL)
                    self.butAnalyze.config(state=NORMAL)
                    self.butGetDateTime.config(state=NORMAL)
                    self.e1.config(state=NORMAL)
                    self.butConnect.config(text="Connect")
            else:
                self.selectCOM = self.cbox01.get()
                self.selectBAUD = self.cbox02.get()
                self.selectDATA = self.cbox03.get()
                self.selectPARITY = self.cbox04.get()
                self.selectSTOP = self.cbox05.get()

                print(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)

                result = self.con.connectSerial(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)

                if(result):
                    #print(" disconnected \n")   
                    self.printTextArea("\nConnected Successfully \n")
                    self.butConnect.config(text="Disconnect")
                    self.cbox01.config(state=DISABLED)
                    self.butAutoRearm.config(state=NORMAL)
                    self.butConnect.config(state=NORMAL)
                    self.butExcelExport.config(state=NORMAL)
                    self.butAnalyze.config(state=NORMAL)
                    self.butGetRawData.config(state=NORMAL)
                    self.butSetRecordDelay.config(state=NORMAL)
                    self.e1.config(state=NORMAL)
                    self.butGetDateTime.config(state=NORMAL)
                    self.butSetDateTime.config(state=NORMAL)
                    self.butDeleteRecordData.config(state=NORMAL)
                    try:
                        self.readFlag = True
                        thread = threading.Thread(target=self.continuousRead)
                        thread.daemon = True
                        thread.start()                    
                    except Exception as err:
                        logging.error(traceback.format_exc())
                        self.readFlag = False
                else:
                    self.printTextArea("\n... Error (Try Again)\n")
                    self.cbox01.config(state=NORMAL)
                    self.butConnect.config(state=NORMAL)
                    self.butAutoRearm.config(state=NORMAL)
                    self.butExcelExport.config(state=NORMAL)
                    self.butAnalyze.config(state=NORMAL)
                    self.butGetDateTime.config(state=NORMAL)
                    self.e1.config(state=NORMAL)
                    
        except Exception as err:
            logging.error(traceback.format_exc())
            self.cbox01.config(state=NORMAL)
        
        finally:            
            self.butConnect.config(state=NORMAL)
            self.butAutoRearm.config(state=NORMAL)
            self.butExcelExport.config(state=NORMAL)
            self.butAnalyze.config(state=NORMAL)
            self.butGetDateTime.config(state=NORMAL)
            self.e1.config(state=NORMAL)
        

    def autoConnect(self):
        self.normalDisable(0)
        var01 = None
        print("autoConnect")
        
        self.cbox01.config(state=NORMAL)
        self.butConnect.config(state=NORMAL)
        self.butAutoRearm.config(state=NORMAL)
        self.butExcelExport.config(state=NORMAL)
        self.butAnalyze.config(state=NORMAL)
        self.butGetDateTime.config(state=NORMAL)
        self.e1.config(state=NORMAL)
        
        for var in range(len(self.comlist)):
            self.selectCOM = self.cbox01.get()
            self.selectBAUD = self.cbox02.get()
            self.selectDATA = self.cbox03.get()
            self.selectPARITY = self.cbox04.get()
            self.selectSTOP = self.cbox05.get()
            print(var)

            self.cbox01.current(var)
            print(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)
            result = False

            if(self.selectCOM != "None"):
                result = self.con.connectSerial(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)
                print(result)
                if(result):
                    self.con.scr.write(b'1')
                    
                    var02 = ""
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    var01 = self.con.readSerial()
                    if(var01 != None):
                        print(str(var01.hex()))
                        var02 = var02 + str(var01.hex())
                    
                    if(var02 != ""):
                        print("var02: " + var02)
                        patternReadingFreq1A = re.compile("fffefdfcf1", re.S | re.M)
                        result = patternReadingFreq1A.search(var02)
                        if(result):
                            self.cbox01.current(var-1)
                            print(" connected \n")
                            if(self.con.scr.isOpen()):
                                result = self.con.disconnectSerial()
                                print(" disconnected \n")
                            
                            self.connectCOM()
                            break
                else:
                    print(str(var) + " not connected \n")
                    self.autoConnectText = 0

                if(self.con.scr.isOpen()):
                    result = self.con.disconnectSerial()
                    print(" disconnected \n")   
        

    def outputGUI(self):
        #------------------------------------------------------Configure Serial Port
        self.scr = serial.Serial()
        self.selectedCom = "None"
        self.selectedBaud = "9600"
        self.selectedData = "8"
        self.selectedParity = "None"
        self.selectedStop = "1"

        
        self.lbl01 = Label(self.frame, text="Gamma")
        self.lbl01.grid(row=0, column=0, sticky=E+W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        Grid.columnconfigure(self.frame, 0, weight=10)
        
        self.lbl02 = Label(self.frame, text="Register")
        self.lbl02.grid(row=0, column=1, sticky=E+W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        Grid.columnconfigure(self.frame, 1, weight=10)
        
        self.lbl03 = Label(self.frame, text="Data")
        self.lbl03.grid(row=0, column=2, sticky=E+W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        Grid.columnconfigure(self.frame, 2, weight=10)
        
        self.lbl04 = Label(self.frame, text="-")
        self.lbl04.grid(row=0, column=3, sticky=W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        #Grid.columnconfigure(self.frame, 2, weight=1) 
        
        self.lbl05 = Label(self.frame, text="Logger")
        self.lbl05.grid(row=0, column=4, sticky=E+W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        Grid.columnconfigure(self.frame, 4, weight=1)
        
        self.lbl06 = Label(self.frame, text="settings")
        self.lbl06.grid(row=0, column=5, sticky=E+W)
        #Grid.rowconfigure(self.frame, 0,weight=1)
        Grid.columnconfigure(self.frame, 5, weight=1)

        self.txtArea = Text(self.frame, height = 10, width=10, state=NORMAL)
        self.vsb = Scrollbar(self.frame, orient="vertical", command=self.txtArea.yview)
        self.txtArea.configure(yscrollcommand=self.vsb.set)
        self.vsb.grid(row=1, column=3, rowspan=20, sticky=N+S+W)
        #Grid.rowconfigure(self.frame, 1,weight=1)
        #Grid.columnconfigure(self.frame, 3, weight=1)
        self.txtArea.grid(row=1, column=0, rowspan=21, columnspan=3, sticky=W + E + N + S, pady=1)
        Grid.rowconfigure(self.frame, 1,weight=1)
        Grid.columnconfigure(self.frame, 0, weight=1)
        self.txtArea.insert(END, "*************************************************************"
                            "\nReArm Gamma Register for fresh installation:"
                            "\n After auto-connect/manual-connect"
                            "\n 1. Get Raw Data"
                            "\n 2. Save Raw Data"
                            "\n 3. Set Acquisition Time"
                            "\n 4. Get DateTime"
                            "\n 5. Set DateTime"
                            "\n 6. Delete GR Data"
                            "\n 7. Close"
                            "\n\nNote: During delete 'All' data, GR is busy for 15-20 min.\n"
                            " At this time connection with GR is not possible.\n Kindly wait or run the Software again after 20 min."
                            "\n**************************************************************\n")
        self.txtArea.see("end")
        self.txtArea.update()
        self.txtArea.config(state=DISABLED)

        #--------------------------------------------------------------------------------------------
        self.lfUSB = LabelFrame(self.frame, text="USB PORT")
        self.lfUSB.grid(row=1, column=4, columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
        Grid.rowconfigure(self.frame, 1,weight=1)
        Grid.columnconfigure(self.frame, 4, weight=1)


        Label(self.lfUSB, text="COM").grid(row=2, column=4, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 2,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)

        self.serial01 = gammaSerial.ListSerialPorts()
        self.comlist = self.serial01.serial_ports()
        self.comlist.append("None")


        self.cbox01 = Combobox(self.lfUSB, values=self.comlist)
        self.cbox01.current([self.comlist.__len__() - 1])
        self.cbox01.grid(row=2, column=5, columnspan=2, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 2,weight=1)
        Grid.columnconfigure(self.lfUSB, 5, weight=1)
        self.cbox01.config(state=NORMAL)

        

        Label(self.lfUSB, text="BAUD(bps)").grid(row=3, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 3,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)

        self.cbox02 = Combobox(self.lfUSB, values=['110','300','600','1200', '2400', '4800', '9600', '14400', '19200', '38400', '56000', '57600', '115200', '128000', '256000'])
        self.cbox02.current([12])
        self.cbox02.grid(row=3, column=5, columnspan=2, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 3,weight=1)
        Grid.columnconfigure(self.lfUSB, 5, weight=1)
        self.cbox02.config(state=DISABLED)

        Label(self.lfUSB, text="DATA").grid(row=4, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 4,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)

        self.cbox03 = Combobox(self.lfUSB, values=['5', '6', '7', '8'])
        self.cbox03.current([3])
        self.cbox03.grid(row=4, column=5, columnspan=2, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 4,weight=1)
        Grid.columnconfigure(self.lfUSB, 5, weight=1)
        self.cbox03.config(state=DISABLED)
        
        Label(self.lfUSB, text="PARITY").grid(row=5, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 5,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)

        self.cbox04 = Combobox(self.lfUSB, values=['Even', 'Mark', 'None', 'Odd', 'Space'])
        self.cbox04.current([2])
        self.cbox04.grid(row=5, column=5, columnspan=2, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 5,weight=1)
        Grid.columnconfigure(self.lfUSB, 5, weight=1)
        self.cbox04.config(state=DISABLED)

        Label(self.lfUSB, text="STOP").grid(row=6, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 6,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)

        self.cbox05 = Combobox(self.lfUSB, values=['1', '1.5', '2'])
        self.cbox05.current([0])
        self.cbox05.grid(row=6, column=5, columnspan=2, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 6,weight=1)
        Grid.columnconfigure(self.lfUSB, 5, weight=1)
        self.cbox05.config(state=DISABLED) 

        #--------------------------------------------------------------------------------------------

        #--------------------------------------------------------------------------------------Connect Button
        self.butConnect = Button(self.lfUSB, text="Connect")
        self.butConnect.config(command=self.connectCOM)
        self.butConnect.grid(row=7, column=4, columnspan=3, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lfUSB, 7,weight=1)
        Grid.columnconfigure(self.lfUSB, 4, weight=1)
        

        #Label(root, text="").grid(row=8, column=0, columnspan = 3, sticky=W + E + N + S, padx=10, pady=3)
        #---------------------------------------------------------------------------------------------------------

        #--------------------------------------------------------------------------------------Get Raw Data Button
        self.lbGET = LabelFrame(self.frame, text="GET DATA")
        self.lbGET.grid(row=9, column=4, columnspan = 3, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.frame, 9,weight=1)
        Grid.columnconfigure(self.frame, 4, weight=1)


        self.butGetRawData = Button(self.lbGET, text="Get Raw Data")

        def getRawData():
            try:
                self.normalDisable(0)
                
                successBool = False                
                
                if(self.con.scr.isOpen()):
                    self.printTextArea("\n\nStart Raw Data Reading ...\n")
                    self.getRawDataText = ""
                    self.con.scr.write(b'6')
                    self.getRawDataFlag = 1
                    successBool = True
    
            except Exception as err:
                logging.error(traceback.format_exc())
                successBool = False

            finally:                    
                if(successBool):
                    None
                else:
                    self.printTextArea("\n... Error (Try Again)\n")
                    
                self.normalDisable(1)
                return successBool

        self.butGetRawData.config(command=getRawData)
        self.butGetRawData.grid(row=10, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbGET, 10,weight=1)
        Grid.columnconfigure(self.lbGET, 4, weight=1)

        #--------------------------------------------------------------------------------------Save Raw Data Button
        self.butSaveRawData = Button(self.lbGET, text="Save Raw Data")

        def saveRawData():
            self.normalDisable(0)
            successBool = False
            try:
                fDialog = tkinter.filedialog.asksaveasfile(mode='w', defaultextension=".txt")
                if(fDialog != None):
                    try:
                        f = open(fDialog.name, "w+")
                        f.write(self.getRawDataText.replace("'b'", ""))
                        self.getRawDataText = ""
                        self.getRawDataFlag = 0
                        successBool = True
                    finally:
                        f.flush()
                        if (f.closed):
                            print("f is closed")
                        else:
                            f.close()
                else:
                    successBool = False
    
            except Exception as err:
                logging.error(traceback.format_exc())                
                successBool = False

            finally:
                if(successBool):
                    self.printTextArea("\n data saved in file " + fDialog.name + " \n")
                else:
                    self.printTextArea("\n... Data Saving Unsuccessful (Try Again)\n")
                    
                self.normalDisable(1)
                return successBool

        self.butSaveRawData.config(command=saveRawData, state=DISABLED)
        self.butSaveRawData.grid(row=10, column=5, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbGET, 10,weight=1)
        Grid.columnconfigure(self.lbGET, 5, weight=1)

        #--------------------------------------------------------------------------------------Clear Screen
        self.butClearScreen = Button(self.lbGET, text="Clear Screen")

        def clearScreen():
            #self.normalDisable(0)
            successBool = False
            try:
                self.txtArea.config(state=NORMAL)
                self.txtArea.delete(1.0, END)
                self.txtArea.see("end")
                self.txtArea.update()
                self.txtArea.config(state=DISABLED)
                successBool = True
    
            except Exception as err:
                logging.error(traceback.format_exc())                
                successBool = False

            finally:
                if(successBool):
                    self.printTextArea("\n Cleared \n")
                else:
                    self.printTextArea("\n... Not Cleared (Try Again)\n")
                    
                #self.normalDisable(1)
                return successBool

        self.butClearScreen.config(command=clearScreen, state=NORMAL)
        self.butClearScreen.grid(row=11, column=5, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbGET, 10,weight=1)
        Grid.columnconfigure(self.lbGET, 5, weight=1)

        #--------------------------------------------------------------------------------------Analyze Data Button
        #self.butAnalyze = Button(self.lbGET, text="Analyze Data")
        
        def doOldAnalysis():
            try:
                self.normalDisable(0)
                self.printTextArea("\n\nStart Analyzing ...\n")
                successBool = False
                dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')
                oldDateTime = dateTimeData
                presetDelay = 0
                fDialog = tkinter.filedialog.askopenfilename(title="Please select a [xyz]_HRM.txt file")
                

                if os.path.exists(fDialog):
                    try:
                        self.printTextArea("\nRead file from: %s ............\n" % fDialog)
                        f0 = open(fDialog, "r+")
                        f1 = open(fDialog[:-4] + "_Analyzed.txt", "w+")

                        var = f0.read()
                        f0.close()
                        print(len(var))
                        i = 0
                        outputStr = ""
                        #dateTime = ["01","01","01","01","01"]
                        dateTimeCount = 0
                        dataCount = 0
                        dataValue = [0, 0, 0, 0, 0]
                        dataIndex = 0
                        crc8Calculator = gammaCRC.crc_Calculate
                        doseValue = 0

                        while(i < (len(var) - 1)):
                            strTwo = var[i] + var[i+1]
                            intTwo = int(strTwo, 16)
                            strPrint01 = "i:" + str(i) + " strTwo:" + strTwo + " intTwo:" + str(intTwo) + " chrTwo:" + chr(intTwo)  + "\n"
                            #print(strPrint01)
                            #f1.write(strPrint01)
                            
                            if(intTwo == 82):
                                if(dateTimeCount == 0):
                                    dateTimeCount = dateTimeCount + 1
                                else:
                                    dateTimeCount = 0
                                    dateTimeCount = dateTimeCount + 1
                            elif(intTwo == 83):
                                if((dateTimeCount == 1) | (dateTimeCount == 2)):
                                    dateTimeCount = dateTimeCount + 1
                                else:
                                    dateTimeCount = 0
                            elif(intTwo == 68):
                                if(dateTimeCount == 3):
                                    dateTimeCount = dateTimeCount + 1
                                else:
                                    dateTimeCount = 0


                            if(dateTimeCount == 4):                            
                                if((i + 21) < len(var)):
                                    strDD01 = var[i+2] + var[i+3]
                                    intDD01 = int(strDD01, 16)                               
                                    strDD02 = var[i+4] + var[i+5]
                                    intDD02 = int(strDD02, 16)
                                    strMM01 = var[i+6] + var[i+7]
                                    intMM01 = int(strMM01, 16)
                                    strMM02 = var[i+8] + var[i+9]
                                    intMM02 = int(strMM02, 16)
                                    strYY01 = var[i+10] + var[i+11]
                                    intYY01 = int(strYY01, 16)
                                    strYY02 = var[i+12] + var[i+13]
                                    intYY02 = int(strYY02, 16)
                                    strhh01 = var[i+14] + var[i+15]
                                    inthh01 = int(strhh01, 16)
                                    strhh02 = var[i+16] + var[i+17]
                                    inthh02 = int(strhh02, 16)
                                    strmm01 = var[i+18] + var[i+19]
                                    intmm01 = int(strmm01, 16)
                                    strmm02 = var[i+20] + var[i+21]
                                    intmm02 = int(strmm02, 16)
                                     
                                    strDD = chr(intDD01) + chr(intDD02)
                                    strMM = chr(intMM01) + chr(intMM02)
                                    strYY = chr(intYY01) + chr(intYY02)
                                    strhh = chr(inthh01) + chr(inthh02)
                                    strmm = chr(intmm01) + chr(intmm02)
                                    dateTimeFormat = strDD + "-" + strMM + "-" + strYY + "-" + strhh + "-" + strmm
                                    try:
                                        dateTimeData = datetime.datetime.strptime(dateTimeFormat, '%d-%m-%y-%H-%M')
                                    except Exception as err:
                                        logging.error(traceback.format_exc())
                                        dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')

                
                                   
                                    strPrint02 = "DATE: " + strDD01 + strDD02 + "-" + str(intDD01) + str(intDD02) + "-" + chr(intDD01) +  chr(intDD02) + "\n"
                                    strPrint03 = "MONTH: " + strMM01 + strMM02 + "-" + str(intMM01) + str(intMM02) + "-" + chr(intMM01) +  chr(intMM02) + "\n"
                                    strPrint04 = "YEAR: " + strYY01 + strYY02 + "-" + str(intYY01) + str(intYY02) + "-" + chr(intYY01) +  chr(intYY02) + "\n"
                                    strPrint05 = "HOUR: " + strhh01 + strhh02 + "-" + str(inthh01) + str(inthh02) + "-" + chr(inthh01) +  chr(inthh02) + "\n"
                                    strPrint06 = "MINUTE: " + strmm01 + strmm02 + "-" + str(intmm01) + str(intmm02) + "-" + chr(intmm01) +  chr(intmm02) + "\n"
                                    
                                    #print(dateTimeFormat + " : " + str(dateTimeData) + " : " + strPrint02 + strPrint03 + strPrint04 + strPrint05 + strPrint06)
                                    #f1.write(dateTimeFormat + " : " + str(dateTimeData) + " : " + strPrint02 + strPrint03 + strPrint04 + strPrint05 + strPrint06)
                                   
                                    i = i + (21-1)
                                    
                                dateTimeCount = 0
                                dataIndex = 2

                            
                            netDateTime = (dateTimeData - oldDateTime).total_seconds()/60.0
                        
                            #print(str(oldDateTime) + "old date \n")
                            #print(str(dateTimeData) + "date time data\n")
                            #print(str(netDateTime) + "net date \n")
                            oldDateTime = dateTimeData

                            if(netDateTime > 0):
                                if(netDateTime < 1576801):
                                    if(netDateTime < 6):
                                        presetDelay = 1
                                    elif(netDateTime < 25):
                                        presetDelay = 10
                                    elif(netDateTime < 65):
                                        presetDelay = 30
                                    else:
                                        presetDelay = netDateTime
                            

                            #print(dataIndex)
                            dataValue[dataIndex] = intTwo
                            dataIndex = dataIndex + 1
                            if(dataIndex >=5):
                                msg = [dataValue[0], dataValue[1], dataValue[2], dataValue[3]]
                                crcResult = crc8Calculator.crc8(msg)
                                if(crcResult == dataValue[4]):
                                    doseValue = (dataValue[0] * math.pow(16,3)) + (dataValue[1] * math.pow(16,2)) + (dataValue[2] * math.pow(16,1)) + (dataValue[3] * math.pow(16,0))
                                    int_data = doseValue
                                    #print(doseValue)
                                    #print(crcResult)
                                    #print("crc matched")
                                    outputStr = "Date-Time: " + str(dateTimeData) + " Delay: " + str(presetDelay) + " min Counts: " + str(int(int_data)) + " cpm Field: " + str(round(int_data*0.833, 2)) + " uR/h "
                                    f1.write(outputStr)
                                    f1.write("\n")
                                    print(outputStr + "\n")
                                    self.printTextArea(outputStr + "\n")
                                    dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                    dataIndex = 0
                                else:
                                    dataValue[0] = dataValue[1]
                                    dataValue[1] = dataValue[2]
                                    dataValue[2] = dataValue[3]
                                    dataValue[3] = dataValue[4]
                                    dataIndex = 4                                
                            
                                
                            i = i+2

                        f1.close()
                        successBool = True
                    finally:
                        if (f0.closed):
                            print("f0 is closed")
                        else:
                            f0.close()

                        if (f1.closed):
                            print("f1 is closed")
                        else:
                            f1.close()
                else:
                    successBool = False
                   
            except Exception as err:
                logging.error(traceback.format_exc())
                successBool = False

            finally:
                if (successBool):
                    self.printTextArea("\n... Data Analyzing Successful %s_Analyzed.txt \n" % fDialog[:-4])
                else:
                    self.printTextArea("\n... Data Analyzing Unsuccessful (Try Again)\n")

                self.normalDisable(1)
                return successBool 

        #self.butAnalyze.config(command = doAnalysis)
        #self.butAnalyze.grid(row = 11, column=4, sticky=W+E, padx=10, pady=3)
        #Grid.rowconfigure(self.lbGET, 11,weight=1)
        #Grid.columnconfigure(self.lbGET, 4, weight=1)

        #--------------------------------------------------------------------------------------Analyze Data Button
        self.butAnalyze = Button(self.lbGET, text="Analyze Data")
        
        def doAnalysis():
            try:
                self.normalDisable(0)
                self.printTextArea("\n\nStart Analyzing ...\n")
                successBool = False
                dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')
                oldDateTime = dateTimeData
                presetDelay = 0
                fDialog = tkinter.filedialog.askopenfilename(title="Please select a [xyz]_HRM.txt file")
                

                if os.path.exists(fDialog):
                    try:
                        self.printTextArea("\nRead file from: %s ............\n" % fDialog)
                        f0 = open(fDialog, "r+")
                        f1 = open(fDialog[:-4] + "_Analyzed.txt", "w+")

                        var = f0.read()
                        #f0.close()
                        print(len(var))
                        i = 0
                        outputStr = ""
                        #dateTime = ["01","01","01","01","01"]
                        dateTimeCount = 0
                        dataCount = 0
                        dataValue = [0, 0, 0, 0, 0]
                        dataIndex = 0
                        crc8Calculator = gammaCRC.crc_Calculate
                        doseValue = 0
                        presetDelay = 0
                        oneMinStart = 1

                        while(i < (len(var) - 1)):
                            strTwo = var[i] + var[i+1]
                            intTwo = int(strTwo, 16)
                            strPrint01 = "i:" + str(i) + " strTwo:" + strTwo + " intTwo:" + str(intTwo) + " hexTwo:" + hex(intTwo)  + "\n"
                            print(strPrint01)
                            #f1.write(strPrint01)

                            dataValue[dataIndex] = intTwo
                            dataIndex = dataIndex + 1

                            
                            
                            if(dataIndex >= 5):
                                print(dataValue)
                                if((dataValue[0] == 0xff) & (dataValue[1] == 0xfe) & (dataValue[2] == 0xfd) & (dataValue[3] == 0xfc)):
                                    dataIndex = 0
                                    if(dataValue[4] == 0xF6):
                                        print("Data Reading Starts")
                                        self.printTextArea("\n Data Reading Starts \n")
                                    elif(dataValue[4] == 0xfa):
                                        print("Data Reading Ends")
                                        self.printTextArea("\n Data Reading Ends \n")
                                    elif(dataValue[4] == 0xf1):
                                        print("one minute start")
                                        oneMinStart = 1
                                        print(oneMinStart)
                                    elif(dataValue[4] == 0x01):
                                        presetDelay = 1
                                        print("time = 1 min")
                                        self.printTextArea("\n time = 1 min \n")
                                    elif(dataValue[4] == 0x0a):
                                        presetDelay = 10
                                        print("time = 10 min")
                                        self.printTextArea("\n time = 10 min \n")
                                    elif(dataValue[4] == 0x1e):
                                        presetDelay = 30
                                        print("time = 30 min")
                                        self.printTextArea("\n time = 30 min \n")

                                    if((i+21) <= (len(var)-1)):                                    
                                        strDD01 = var[i+2] + var[i+3]
                                        intDD01 = int(strDD01, 16)                               
                                        strDD02 = var[i+4] + var[i+5]
                                        intDD02 = int(strDD02, 16)
                                        strMM01 = var[i+6] + var[i+7]
                                        intMM01 = int(strMM01, 16)
                                        strMM02 = var[i+8] + var[i+9]
                                        intMM02 = int(strMM02, 16)
                                        strYY01 = var[i+10] + var[i+11]
                                        intYY01 = int(strYY01, 16)
                                        strYY02 = var[i+12] + var[i+13]
                                        intYY02 = int(strYY02, 16)
                                        strhh01 = var[i+14] + var[i+15]
                                        inthh01 = int(strhh01, 16)
                                        strhh02 = var[i+16] + var[i+17]
                                        inthh02 = int(strhh02, 16)
                                        strmm01 = var[i+18] + var[i+19]
                                        intmm01 = int(strmm01, 16)
                                        strmm02 = var[i+20] + var[i+21]
                                        intmm02 = int(strmm02, 16)
                                         
                                        strDD = chr(intDD01) + chr(intDD02)
                                        strMM = chr(intMM01) + chr(intMM02)
                                        strYY = chr(intYY01) + chr(intYY02)
                                        strhh = chr(inthh01) + chr(inthh02)
                                        strmm = chr(intmm01) + chr(intmm02)
                                        dateTimeFormat = strDD + "-" + strMM + "-" + strYY + "-" + strhh + "-" + strmm
                                        print(strDD01 + strDD02 + strMM01 + strMM02 + strYY01 + strYY02 + strhh01 + strhh02 + strmm01 + strmm02)
                                        try:
                                            dateTimeData = datetime.datetime.strptime(dateTimeFormat, '%d-%m-%y-%H-%M')
                                            if(oneMinStart == 1):
                                                #one minute correction. When one minute set to GR, timestamp saved in memory, timer = 0 and data come after one min. but software consider memory saved timestamp for 1st data.
                                                #print(dateTimeData)
                                                dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                                self.printTextArea("One min added\n")
                                                #print(dateTimeData)
                                        except Exception as err:
                                            logging.error(traceback.format_exc())
                                            dateTimeData = datetime.datetime.strptime("0101010101", '%d%m%y%H%M')
                                            
                                        i=i+20
                                else:
                                    msg = [dataValue[0], dataValue[1], dataValue[2], dataValue[3]]
                                    crcResult = crc8Calculator.crc8(msg)
                                    oneMinStart = 0 
                                    if(crcResult == dataValue[4]):
                                        doseValue = (dataValue[0] * math.pow(256,3)) + (dataValue[1] * math.pow(256,2)) + (dataValue[2] * math.pow(256,1)) + (dataValue[3] * math.pow(256,0))
                                        int_data = doseValue
                                        #print(doseValue)
                                        #print(crcResult)
                                        print("crc matched")                                    
                                        outputStr = "Date-Time: " + str(dateTimeData) + " Delay: " + str(presetDelay) + " min Counts: " + str(int_data) + " cpm Field: " + str(round(int_data*0.833, 2)) + " uR/h "
                                        f1.write(outputStr)
                                        self.printTextArea(outputStr + "\n")
                                        f1.write("\n")
                                        #print(outputStr + "\n")
                                        dateTimeData = dateTimeData + datetime.timedelta(minutes=1)
                                        dataIndex = 0
                                        presetDelay = 1
                                    else:
                                        dataValue[0] = dataValue[1]
                                        dataValue[1] = dataValue[2]
                                        dataValue[2] = dataValue[3]
                                        dataValue[3] = dataValue[4]
                                        dataIndex = 4

                                    
                            i = i+2

                        f0.close()
                        f1.close()
                        successBool = True
                    finally:
                        if (f0.closed):
                            print("f0 is closed")
                        else:
                            f0.close()

                        if (f1.closed):
                            print("f1 is closed")
                        else:
                            f1.close()
                else:
                    successBool = False
                   
            except Exception as err:
                logging.error(traceback.format_exc())
                successBool = False

            finally:
                if (successBool):
                    self.printTextArea("\n... Data Analyzing Successful %s_Analyzed.txt \n" % fDialog[:-4])
                else:
                    self.printTextArea("\n... Data Analyzing Unsuccessful (Try Again)\n")

                self.normalDisable(1)
                return successBool 

        self.butAnalyze.config(command = doAnalysis)
        self.butAnalyze.grid(row = 11, column=4, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lbGET, 11,weight=1)
        Grid.columnconfigure(self.lbGET, 4, weight=1)


        #--------------------------------------------------------------------------------------Excel Export Button
        self.butExcelExport = Button(self.lbGET, text="Excel Export")
        def doExcelExport():
            try:
                self.normalDisable(0)
                self.printTextArea("\n\nStart Exporting ...\n")
                successBool = False

                fDialog = tkinter.filedialog.askopenfilename(title="Please select a [xyz]_Analyzed.txt file")

                if os.path.exists(fDialog):
                    try:
                        self.printTextArea("\nReading file from: %s ............\n" % fDialog)

                        f0 = open(fDialog, "r+")
                        var = f0.read()
                        sep = var.split(" ")
                        f0.close()
                        
                        workbook = xlsxwriter.Workbook(fDialog[:-4] + ".xlsx")
                        print("\nworkbook: " + str(workbook))
                        worksheet = workbook.add_worksheet()
                        print("\nworksheet: " + str(worksheet))
                        
                        txtLine = ""
                        rowCount = 0
                        colCount = 0

                        trackRC = 0

                        for txt in sep:
                            self.printTextArea(txt + "\t")
                            
                            if((colCount == 4) | (colCount == 7)):
                                worksheet.write(rowCount, colCount, float(txt))
                            elif(colCount == 10):
                                worksheet.write(rowCount, colCount, float(txt))
                            else:
                                result = worksheet.write(rowCount, colCount, txt)
                                    
                            if(trackRC > 10):
                                trackRC = 0              
                                colCount = 0
                                rowCount = rowCount + 1
                            else:
                                trackRC = trackRC + 1                       
                                colCount = colCount + 1

                        successBool = True
                    finally:
                        if (f0.closed):
                            print("f0 is closed")
                        else:
                            f0.close()

                else:
                    successBool = False

            except Exception as err:
                successBool = False
                logging.error(traceback.format_exc())
                
            finally:                    
                if (successBool):
                    self.printTextArea("\n... Data Excel Exported Successful " + fDialog[:-4] + ".xlsx \n")
                else:
                    self.printTextArea("\n... Data Excel Exported Unsuccessful (Try Again)\n")

                self.normalDisable(1)
                return successBool

        self.butExcelExport.config(command = doExcelExport)
        self.butExcelExport.grid(row=12, column=4, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbGET, 12,weight=1)
        Grid.columnconfigure(self.lbGET, 4, weight=1)

        #Label(root, text="").grid(row=13, column=0, sticky=W + E + N + S, padx=3, pady=3)
        #---------------------------------------------------------------------------------------------------------

        self.lbSET = LabelFrame(self.frame, text="SET DATA")
        self.lbSET.grid(row=14, column=4, columnspan=3, sticky=W+E+N+S, padx=10, pady=3 )
        Grid.rowconfigure(self.frame, 14,weight=1)
        Grid.columnconfigure(self.frame, 4, weight=1)

        self.recordTime = StringVar()
        self.recordTime.set(1)
        self.recordTimeSelected = self.recordTime.get()

        self.rbSetRecordTime01 = Radiobutton(self.lbSET, text= "01 min", value=1, variable=self.recordTime)
        self.rbSetRecordTime01.grid(row=15, column = 4, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 15,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)

        self.rbSetRecordTime02 = Radiobutton(self.lbSET, text= "10 min", value=2, variable=self.recordTime)
        self.rbSetRecordTime02.config()
        self.rbSetRecordTime02.grid(row=15, column = 5, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 15,weight=1)
        Grid.columnconfigure(self.lbSET, 5, weight=1)

        self.rbSetRecordTime03 = Radiobutton(self.lbSET, text= "30 min", value=3, variable=self.recordTime)
        self.rbSetRecordTime03.config()
        self.rbSetRecordTime03.grid(row=15, column = 6, sticky=W+E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 15,weight=1)
        Grid.columnconfigure(self.lbSET, 6, weight=1)

        #--------------------------------------------------------------------------------------Set Acquisition Time Button
        self.butSetRecordDelay = Button(self.lbSET, text="Set Acquisition Time ...")

        def setRecordDelay():
            try:
                self.normalDisable(0)
                successBool = False

                if(self.con.scr.isOpen()):
                    self.radioGetTest = int(self.recordTime.get())
                    radioTime = 0
                    byteSend = b'0'
                    if(self.radioGetTest == 1):
                        radioTime = 1
                        byteSend = b'1'
                    elif(self.radioGetTest == 2):
                        radioTime = 10
                        byteSend = b'2'
                    elif(self.radioGetTest == 3):
                        radioTime = 30
                        byteSend = b'3'
                    dateWarning = messagebox.askyesno("Date Warning", "Do you want to set Acquisition Time " + str(radioTime) + " min in GR?")
                    if(dateWarning):                        
                        self.printTextArea("\n\n Start Setting Acquisition Time " + str(radioTime) + " min ...\n")
                        self.getRawDataFlag = 0
                        self.getRawDataText = ""
                        self.con.scr.write(byteSend)
                        print(self.radioGetTest)
                        successBool = True
                    else:
                        successBool = True

            except Exception as err:
                logging.error(traceback.format_exc())                
                successBool = False
            finally:
                if(successBool):
                    None
                else:
                    self.printTextArea("\n... Error (Try Again)\n")
                    
                self.normalDisable(1)
                return successBool


        self.butSetRecordDelay.config(command= setRecordDelay)
        self.butSetRecordDelay.grid(row=16, column=4, columnspan=3, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 16,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)

        #--------------------------------------------------------------------------------------Get DateTime Button
        self.now01 = datetime.datetime.now()
        self.strVar = str(self.now01)
        self.modVar = self.strVar[0:17] + "01"
        self.e1 = Entry(self.lbSET)
        self.e1.insert(0, self.modVar)
        self.e1.grid(row=17, column=4, columnspan=3, sticky= W+E, padx=10, pady=10)
        Grid.rowconfigure(self.lbSET, 17,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)
        self.temp05 = self.e1.get()

        self.butGetDateTime = Button(self.lbSET, text="Get DateTime")
        def getDateTimeData():
            now01 = datetime.datetime.now()
            strVar = str(now01)
            modVar = strVar[0:17] + "01"
            self.e1.icursor(0)
            self.e1.delete(0,30)
            self.e1.insert(0, modVar)

        self.butGetDateTime.config(command=getDateTimeData)
        self.butGetDateTime.grid(row=18, column=4, columnspan=1, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 18,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)


        #--------------------------------------------------------------------------------------Set DateTime Button
        self.butSetDateTime = Button(self.lbSET, text="Set DateTime ")
        def setDateTimeData():
            
            try:
                self.normalDisable(0)                
                successBool = False

                if (self.con.scr.isOpen()):

                    dateWarning = messagebox.askyesno("Date Warning", "Do you want to load Date-Time " + str(self.e1.get()) + " in GR?")
                    if (dateWarning):                    
                        now01 = str(self.e1.get())
                        print("now01: " + now01)
                        day01 = now01[8:10]
                        if (len(day01) < 2):
                            day01 = "%s%s" % ("0", day01)
                        month01 = now01[5:7]
                        if ((len(month01) < 2)):
                            month01 = "%s%s" % ("0", month01)
                        year01 =  now01[2:4]
                        if ((len(year01) < 2)):
                            year01 = "%s%s" % ("0", year01)
                        hour01 = now01[11:13]
                        if ((len(hour01) < 2)):
                            hour01 = "%s%s" % ("0", hour01)
                        minute01 = now01[14:16]
                        if ((len(minute01) < 2)):
                            minute01 = "%s%s" % ("0", minute01)
                        second01 = now01[17:19]
                        if ((len(second01) < 2)):
                            second01 = "%s%s" % ("0", second01)
                        dateData = "%s%s%s%s%s%s" % (
                            day01, month01, year01, hour01, minute01, second01)
                        print("datetime data", dateData)

                        self.printTextArea(now01 + "\n")
                        self.printTextArea(dateData + "\n")

                        
                        self.printTextArea("\n\nStart Setting Date Time " + now01 + " ...\n")
                        self.printTextArea("\nKindly wait ...\n")
                        self.getRawDataText = ""
                        self.getRawDataFlag = 0
                        self.con.scr.write(b'4')
                        time.sleep(1)
                        self.con.scr.write(dateData[0].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[1].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[2].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[3].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[4].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[5].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[6].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[7].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[8].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[9].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[10].encode())
                        time.sleep(0.5)
                        self.con.scr.write(dateData[11].encode())

                        successBool = True
                    else:
                        successBool = True

            except Exception as err:
                logging.error(traceback.format_exc())                
                successBool = False
                
            finally:
                if(successBool):
                    None
                else:
                    self.printTextArea("\n... Error (Try Again)\n")
                    
                self.normalDisable(1)
                return successBool

        self.butSetDateTime.config(command=setDateTimeData)
        self.butSetDateTime.grid(row=18, column=5, columnspan=1, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 18,weight=1)
        Grid.columnconfigure(self.lbSET, 5, weight=1)

        #--------------------------------------------------------------------------------------Delete Record Data Button
        self.deleteRecordData = StringVar()
        self.deleteRecordData.set(1)

        self.rbDeleteRecordData = Radiobutton(self.lbSET, text= "Till Now", value=1, variable=self.deleteRecordData)
        self.rbDeleteRecordData.config()
        self.rbDeleteRecordData.grid(row=19, column = 4, sticky=W+E, padx=10, pady=10)
        Grid.rowconfigure(self.lbSET, 19,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)

        self.rbDeleteRecordData = Radiobutton(self.lbSET, text= "ALL", value=2, variable=self.deleteRecordData)
        self.rbDeleteRecordData.config()
        self.rbDeleteRecordData.grid(row=19, column = 5, sticky=W+E, padx=10, pady=10)
        Grid.rowconfigure(self.lbSET, 19,weight=1)
        Grid.columnconfigure(self.lbSET, 5, weight=1)

        self.butDeleteRecordData = Button(self.lbSET, text="Delete GR Data")
        def delRecordData():
            try:
                self.normalDisable(0)
                successBool = False

                if(self.con.scr.isOpen()):
                    delRecordSelected = int(self.deleteRecordData.get())
                    print(delRecordSelected)
                    delRec = None
                    byteSend = b'0'
                    if(delRecordSelected == 1):
                        delRec = "Till Now"
                        delmessage = "\nKindly wait ...\n"
                        byteSend = b'7'
                    elif(delRecordSelected == 2):
                        delRec = "All"
                        delmessage =    "\n\nNote: During delete 'All' data, GR is busy for 15-20 min.\n At this time connection with GR is not possible.\n Kindly wait or run the Software again after 20 min."
                        byteSend = b'5'
                        
                    dateWarning = messagebox.askyesno("Date Warning", "Do you want to delete data: " + delRec + " ? ")
                    if(dateWarning):
                        if(self.con.scr.isOpen()):
                            self.printTextArea("\n\n Start Deleting Data " + delRec + " ...\n")
                            self.printTextArea(delmessage)
                            self.getRawDataFlag = 0
                            self.getRawDataText = ""
                            self.con.scr.write(byteSend)
                            successBool = True
                    else:
                        successBool = True
                        self.normalDisable(1)

            except Exception as err:
                logging.error(traceback.format_exc())                
                successBool = False
                
            finally:
                if(successBool):
                    None
                else:
                    self.printTextArea("\n... Error (Try Again)\n")
                    
                #self.normalDisable(1)
                self.butAutoRearm.config(state=NORMAL)
                self.butConnect.config(state=NORMAL)
                self.butExcelExport.config(state=NORMAL)
                self.butAnalyze.config(state=NORMAL)
                self.butGetDateTime.config(state=NORMAL)
                return successBool

        self.butDeleteRecordData.config(command= delRecordData)
        self.butDeleteRecordData.grid(row=19, column=6, sticky=W + E, padx=10, pady=10)
        Grid.rowconfigure(self.lbSET, 19,weight=1)
        Grid.columnconfigure(self.lbSET, 6, weight=1)

        #--------------------------------------------------------------------------------------

        #Label(lbSET, text="").grid(row=20, column=0, columnspan = 3, sticky=W + E + N + S, padx=10, pady=1)

        #--------------------------------------------------------------------------------------Re-Equip Button
        self.butAutoRearm = Button(self.lbSET, text="Close")
        
        def autoRearm():
            if(self.con.scr.isOpen()):
                result = self.con.disconnectSerial()
                if(result):
                    self.readFlag = False
            self.master.destroy()
            
        self.butAutoRearm.config(command = autoRearm)
        self.butAutoRearm.grid(row=21, column=4, columnspan=3, sticky=W + E, padx=10, pady=3)
        Grid.rowconfigure(self.lbSET, 21,weight=1)
        Grid.columnconfigure(self.lbSET, 4, weight=1)

        #autoConnect()
        #--------------------------------------------------------------------------------------------------------------------------------------------
        #Label(root, text="Copyright @ RMS&DS    {For any issue / suggestion contact: Ashutosh / Govinda , 022-2559-2620}", anchor=CENTER).grid(row=22, column=0,columnspan=4, sticky=W + E + N + S, padx=3, pady=3)
        Label(self.frame, text="Copyright @RMS&DS, RSSD, BARC. In case of queries/suggestions, please contact 'Ashutosh/Govinda'. Phone: +91-22-25592620.", anchor=CENTER).grid(row=22, column=0,columnspan=6, sticky=W + E, padx=3, pady=3)
        #Grid.rowconfigure(self.frame, 22,weight=1)
        Grid.columnconfigure(self.frame, 0, weight=1)
        #-------------------------------------------------------------------------------------------------------------------------------------------------
    

def main():
    root = Tk()
    layer01 = GR_layer01(root)
    root.mainloop()

if __name__ == "__main__":
    main()
