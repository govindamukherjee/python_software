import serial
import time
from time import sleep
import threading
import traceback
import logging

class ComConnect():

    def __init__(self):
        #------------------------------------------------------Configure Serial Port
        self.scr = serial.Serial()
        self.selectCOM = "None"
        self.selectBAUD = "9600"
        self.selectDATA = "8"
        self.selectPARITY = "None"
        self.selectSTOP = "1"
        print(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)
        return None

    def disconnectSerial(self):
        if(self.scr.isOpen()):
            try:
                self.scr.close()
                self.successBool = True
            except Exception as err:
                logging.error(traceback.format_exc())
                print("error closing serial port: ")
                self.successBool = False
            finally:
                return self.successBool
        
    def connectSerial(self, selectCOM, selectBAUD, selectDATA, selectPARITY, selectSTOP):
        self.selectCOM = selectCOM
        self.selectBAUD = selectBAUD
        self.selectDATA = selectDATA
        self.selectPARITY = selectPARITY
        self.selectSTOP = selectSTOP

        print(self.selectCOM, self.selectBAUD, self.selectDATA, self.selectPARITY, self.selectSTOP)

        self.scr.port = self.selectCOM
        self.scr.baudrate = int(self.selectBAUD)

        if (self.selectDATA == '8'):
            self.scr.bytesize = serial.EIGHTBITS
        elif (self.selectDATA == '7'):
            self.scr.bytesize = serial.SEVENBITS
        elif (self.selectDATA == '6'):
            self.scr.bytesize = serial.SIXBITS
        elif (self.selectDATA == '5'):
            self.scr.bytesize = serial.FIVEBITS

        if (self.selectPARITY == 'None'):
            self.scr.parity = serial.PARITY_NONE
        elif (self.selectPARITY == 'Even'):
            self.scr.parity = serial.PARITY_EVEN
        elif (self.selectPARITY == 'Mark'):
            self.scr.parity = serial.PARITY_MARK
        elif (self.selectPARITY == 'Odd'):
            self.scr.parity = serial.PARITY_ODD
        elif (self.selectPARITY == 'Space'):
            self.scr.parity = serial.PARITY_SPACE

        if (self.selectSTOP == '1'):
            self.scr.stopbits = serial.STOPBITS_ONE
        elif (self.selectSTOP == '1.5'):
            self.scr.stopbits = serial.STOPBITS_ONE_POINT_FIVE
        elif (self.selectSTOP == '2'):
            self.scr.stopbits = serial.STOPBITS_TWO

        self.scr.timeout = 1
        self.scr.xonxoff = False
        self.scr.rtscts = False
        self.scr.dsrdtr = False
        self.scr.writeTimeout = 2

        try:
            self.scr.open()
            self.successBool = True
        except Exception as err:
            logging.error(traceback.format_exc())
            print("error open serial port: ")
            self.successBool = False
        finally:
            return self.successBool

    def sendSerial(self, sendData):
        try:
            self.scr.write(sendData.encode('utf-8'))
            self.successBool = True
        except Exception as err:
            logging.error(traceback.format_exc())
            self.successBool = False
        finally:
            return self.successBool

    def readSerial(self):
        self.data_str = None;
        try:
            if(self.scr.inWaiting()>0):                                                 #if incoming bytes are waiting to be read from the serial input buffer
                #self.data_str = self.scr.read(self.scr.inWaiting()).decode('ascii')    #read the bytes and convert from binary array to ASCII
                self.data_str = self.scr.read(self.scr.inWaiting())
                #print(self.data_str);
                #print("----------------\n");
                #print(self.data_str, end = " ")                                        #print the incoming string without putting a new-line ('\n') automatically after every print()
                                                                                        #Put the rest of your code you want here
        
            time.sleep(0.01)                                                            # Optional: sleep 10 ms (0.01 sec) once per loop to let other threads on your PC run during this time.
        except Exception as err:
            logging.error(traceback.format_exc())
            self.successBool = False
        finally:
            self.successBool = True
            if(self.data_str == None):
                return None
            else:
                #return str(self.data_str).replace("b'", "")
                #print("data_str type: ")
                #print(type(self.data_str))
                return self.data_str
                
    def continuousRead(self):
        while(True):
            self.result = self.readSerial()
            if(self.result != None):
                #print(str(self.result) + " continuousRead  \n")
                print(str(self.result).replace(" ", ""))
                


def main():
    con = ComConnect()
    result = con.connectSerial('COM3', '115200', '8', 'None', '1')
    print("connect: " + str(result))
    result = con.sendSerial('6')
    print("sendSerial: " + str(result))
    result = con.readSerial()
    print("readSerial: " + str(result))
    #result = con.sendSerial('6667')
    print("sendSerial: " + str(result))
    
    result = con.readSerial()
    print("readSerial: " + str(result))
    #result = con.sendSerial('6668')
    print("sendSerial: " + str(result))
    try:
        thread = threading.Thread(target=con.continuousRead)
        thread.daemon = True
        thread.start() 
    except Exception as err:
        logging.error(traceback.format_exc())
    time.sleep(10)    
    

if __name__ == "__main__":
    main()
    
