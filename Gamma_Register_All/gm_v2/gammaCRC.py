import traceback
import logging

class crc_Calculate:

    def __init__(self, master):
        None

    def crc8(msg):
        try:
            #print("\n\nStart CRC8 Analyzing ...\n")
            successBool = False
            generator = 0x9B
            crc = 0x00
            #msg = [0x00, 0x00, 0x00, 0x05]
            bitLen = [0, 0, 0, 0, 0, 0, 0 ,0]
            #print(msg)
            for i in msg:
                crc = crc ^ i
                for j in bitLen:
                    if((crc & 0x80) != 0):
                        crc = crc ^ generator
                    crc = crc << 1
            crc =crc >> 1
            #print(crc)
            #print(hex(crc))
            successBool = True

            
        except Exception as err:
            logging.error(traceback.format_exc())
            successBool = False

        finally:
            #print(msg)
            if (successBool):
                #print("\n... CRC8 Calculation Successful \n")
                None
            else:
                #print("\n... CRC8 Calculation Unsuccessful (Try Again)\n")
                None

            return crc
                    
def main():
    var = crc_Calculate
    msg = [0, 0, 0, 0]
    result = var.crc8(msg)
    print(result)
    print(hex(result))
    

if __name__ == "__main__":
    main()


