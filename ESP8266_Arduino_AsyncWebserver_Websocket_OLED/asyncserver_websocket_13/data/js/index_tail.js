var Socket;
var srNo = "20001";

var areaID = 0; //a
var bkgCPM = 0; //b
var grossCnts = 0; //c
var eff = 0; //e
var eventID = "0"; //f
var runMin = 0; //g
var unitSet = 0; //h
var cl = 0; //l
var periodSec = 0; //p
var bkgMin = 0; //u
var samMin = 0; //v
var remarks = "remarks"; //y
var tempC = "";

var areaNo = 0;
var labNo = 0;   //areaID = areaNo*10+labNo
var eventDate = 0;
var eventTime = 0; //eventID = "" + eventDate + "" + eventTime;

var timerID=0;

var Socket;

/* Initiate what has to be done */
function updateSync()
{
  var d = new Date();
  var yr = d.getFullYear() - 2000;
  var mo = d.getMonth()+1;
  var da = d.getDate();
  var hr = d.getHours();
  var mi = d.getMinutes();
  var se = d.getSeconds();

  var eventID = "" + yr;
  if(mo < 10)
  {
    eventID = eventID + "0" + mo;
  }
  else
  {
    eventID = eventID + "" + mo;
  }

  if(da < 10)
  {
    eventID = eventID + "0" + da;
  }
  else
  {
    eventID = eventID + "" + da;
  }

  if(hr < 10)
  {
    eventID = eventID + "0" + hr;
  }
  else
  {
    eventID = eventID + "" + hr;
  }

  if(mi < 10)
  {
    eventID = eventID + "0" + mi;
  }
  else
  {
    eventID = eventID + "" + mi;
  }

  if(se < 10)
  {
    eventID = eventID + "0" + se;
  }
  else
  {
    eventID = eventID + "" + se;
  }
  Socket.send("20001#f#u#" + eventID);
  Socket.send("20001#z#o");
}


function init()
{
  //console.log("init");

  var passCode = prompt("Passcode:", "5");
  if (passCode != "govinda")
  {
    var eventDiv = document.getElementById("bodyDiv");
    eventDiv.style.visibility = "hidden";
  }
  else
  {
    setTimeout(function()
    {
      //console.log("3 sec timer");
      //Socket.send("20001#z#o");
      updateSync();
    }, 1000);
  }

  Socket = new WebSocket('ws://' + window.location.hostname + ':81/');
  Socket.onmessage = function(event)
  {
    //console.log(event.data);
    if((event.data).slice(0, 5) == srNo)  //"srNo#functionCode#for Operation/ for ack only#data" ---------- ex-----"20001#e#a#25"
    {
      if((event.data).slice(6,7) == "a")
      {
        var areaIDReceived = 0;
        if((event.data).slice(8,9) == "o") //operation
        {
          //console.log("areaID");
          areaIDReceived = parseInt((event.data).slice(10));
          if(areaIDReceived == areaID)
          {
            //console.log("areaID Matched");
            updateAreaID(areaIDReceived);
          }
          else
          {
            //console.log("areaID resend");
            Socket.send("20001#a#o#"+areaID);
          }
        }
        else if((event.data).slice(8,9) == "u") //update
        {
          //console.log("areaID Update");
          areaIDReceived = parseInt((event.data).slice(10));
          updateAreaID(areaIDReceived);
        }
        else if((event.data).slice(8,9) == "a") //ack
        {
          //console.log("areaID ack");
        }
      }
      else if((event.data).slice(6,7) == "b")
      {
        var bkgCPMReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("bkgCPM");
          bkgCPMReceived = parseInt((event.data).slice(10));
          if(bkgCPMReceived == bkgCPM)
          {
            //console.log("bkgCPM Matched");
            updatebkgCPM(bkgCPMReceived);
          }
          else
          {
            //console.log("bkgCPM Resend");
            Socket.send("20001#b#o#"+bkgCPM);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("bkgCPM update");
          bkgCPMReceived = parseInt((event.data).slice(10));
          updatebkgCPM(bkgCPMReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("bkgCPM ACK");
        }
      }
      else if((event.data).slice(6,7) == "c")
      {
        if((event.data).slice(8,9) == "u")
        {
          //console.log("grossCnts update");
          grossCnts = parseInt((event.data).slice(10));
          document.getElementById("grossCnts").innerHTML = grossCnts;
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("grossCnts ack");
        }
      }
      else if((event.data).slice(6,7) == "e")
      {
        var effReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("eff");
          effReceived = parseInt((event.data).slice(10));
          if(effReceived == eff)
          {
            //console.log("eff Matched");
            updateEff(effReceived);
          }
          else
          {
            //console.log("eff Resend");
            Socket.send("20001#e#o#"+eff);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("eff update");
          effReceived = parseInt((event.data).slice(10));
          updateEff(effReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("eff ACK");
        }
      }
      else if((event.data).slice(6,7) == "f")
      {
        if((event.data).slice(8,9) == "u")
        {
          //console.log("eventID update");
          eventID = (event.data).slice(10);
          var yr = parseInt(eventID.substring(0,2)) + 2000;
          var mo = parseInt(eventID.substring(2,4));
          var da = parseInt(eventID.substring(4,6));
          var hr = parseInt(eventID.substring(6,8));
          var mi = parseInt(eventID.substring(8,10));
          var se = parseInt(eventID.substring(10,12));
          document.getElementById("eventID").innerHTML = yr + "-" + mo + "-" + da + " " + hr + ":" + mi + ":" + se;
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("eventID ack");
        }
      }
      else if((event.data).slice(6,7) == "g")
      {
        var runMinReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("runMin");
          runMinReceived = parseInt((event.data).slice(10));
          if(runMinReceived == runMin)
          {
            //console.log("runMin Matched");
            updateRunMin(runMinReceived);
          }
          else
          {
            //console.log("runMin Resend");
            Socket.send("20001#g#o#"+runMin);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("runMin update");
          runMinReceived = parseInt((event.data).slice(10));
          updateRunMin(runMinReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("runMin ack");
        }
      }
      else if((event.data).slice(6,7) == "h")
      {
        if((event.data).slice(8,9) == "o")
        {
          var unitSetReceived = 0;
          //console.log("unitSet");
          unitSetReceived = parseInt((event.data).slice(10));
          if(unitSetReceived == unitSet)
          {
            //console.log("unitSet Matched");
            updateUnitSet(unitSetReceived);
          }
          else
          {
            //console.log("unitSet Resend");
            Socket.send("20001#h#o#"+unitSet);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("unitSet update");
          unitSetReceived = parseInt((event.data).slice(10));
          updateUnitSet(unitSetReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("unitSet ack");
        }
      }
      else if((event.data).slice(6,7) == "l")
      {
        var clReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("cl");
          clReceived = parseInt((event.data).slice(10));
          if(clReceived == cl)
          {
            //console.log("cl Matched");
            Socket.send("20001#l#a#"+cl);
            updateCL(clReceived);
          }
          else
          {
            //console.log("cl Resend");
            Socket.send("20001#l#o#"+clTemp);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("cl update");
          clReceived = parseInt((event.data).slice(10));
          updateCL(clReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("cl ack");
        }
      }
      else if((event.data).slice(6,7) == "m")
      {
        if((event.data).slice(8,9) == "u")
        {
          var mem = parseInt((event.data).slice(10));
          document.getElementById("opID").innerHTML = "Mem: ";
          document.getElementById("opValue").innerHTML = mem;
          document.getElementById("mem").innerHTML = mem;

          //console.log("mem update = " + mem);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("mem ack");
        }
      }
      else if((event.data).slice(6,7) == "n")
      {
        if((event.data).slice(8,9) == "o")
        {
          //console.log("nextB");
          Socket.send("20001#n#a");
          document.getElementById("opID").innerHTML = "Next";
          document.getElementById("opValue").innerHTML = "";
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("nextB ack");
        }
      }
      else if((event.data).slice(6,7) == "o")
      {
        if((event.data).slice(8,9) == "o")
        {
          //console.log("Stop");
          Socket.send("20001#o#a");
          document.getElementById("opID").innerHTML = "Stop";
          document.getElementById("opValue").innerHTML = "";
          //remarks = document.getElementById("remarks").value;
          addRow();
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("stopB");
        }
      }
      else if((event.data).slice(6,7) == "p")
      {
        if((event.data).slice(8,9) == "u")
        {
          //console.log("periodSec update");
          periodSec = parseInt((event.data).slice(10));
          document.getElementById("periodSec").innerHTML = periodSec;
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("periodSec ACK");
        }
      }
      else if((event.data).slice(6,7) == "r")
      {
        if((event.data).slice(8,9) == "o")
        {
          //console.log("resetB");
          Socket.send("20001#r#a");
          document.getElementById("opID").innerHTML = "Reset";
          document.getElementById("opValue").innerHTML = "";
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("resetB ACK");
        }
      }
      else if((event.data).slice(6,7) == "s")
      {
        if((event.data).slice(8,9) == "o")
        {
          //console.log("startB");
          Socket.send("20001#s#a");
          document.getElementById("opID").innerHTML = "Start";
          document.getElementById("opValue").innerHTML = "";
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("startB ack");
        }
      }
      else if((event.data).slice(6,7) == "t")
      {
        if((event.data).slice(8,9) == "u")
        {
          //console.log("tempC");
          //Socket.send("20001#s#a");
          tempC = (event.data).slice(10);
          document.getElementById("tempC").innerHTML = tempC;
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("tempC ack");
        }
      }
      else if((event.data).slice(6,7) == "u")
      {
        var bkgMinReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("bkgMin");
          bkgMinReceived = parseInt((event.data).slice(10));
          if(bkgMinReceived == bkgMin)
          {
            //console.log("bkgMin Matched");
            updateBkgMin(bkgMinReceived);
          }
          else
          {
            //console.log("bkgMin Resend");
            Socket.send("20001#u#o#"+bkgMin);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("bkgMin update");
          bkgMinReceived = parseInt((event.data).slice(10));
          updateBkgMin(bkgMinReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("bkgMin ack");
        }
      }
      else if((event.data).slice(6,7) == "v")
      {
        var samMinReceived = 0;
        if((event.data).slice(8,9) == "o")
        {
          //console.log("samMin");
          samMinReceived = parseInt((event.data).slice(10));
          if(samMinReceived == samMin)
          {
            //console.log("samMin Matched");
            updateSamMin(samMinReceived);
          }
          else
          {
            //console.log("samMin Resend");
            Socket.send("20001#v#o#"+samMin);
          }
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("samMin update");
          samMinReceived = parseInt((event.data).slice(10));
          updateSamMin(samMinReceived);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("samMin ack");
        }
      }
      else if((event.data).slice(6,7) == "x")
      {
        if((event.data).slice(8,9) == "u")
        {
          //console.log("log Data Update");
          var logData = (event.data).slice(10);
          logJsonToTable(logData);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("log Data ack");
        }
      }
      else if((event.data).slice(6,7) == "y")
      {
        if((event.data).slice(8,9) == "o")
        {
          //console.log("remarks");
          remarks = (event.data).slice(10);
        }
        else if((event.data).slice(8,9) == "u")
        {
          //console.log("remarks update");
          remarks = (event.data).slice(10);
          document.getElementById("remarks").value = remarks;
        }
        else if((event.data).slice(8,9) == "g") //get  remarks should be < 75 inputs
        {
          //console.log("remarks get ");
          remarks = document.getElementById("remarks").value;
          //console.log(remarks);
          Socket.send("20001#y#o#"+remarks);
        }
        else if((event.data).slice(8,9) == "a")
        {
          //console.log("remarks AKG");
        }
      }
    }
    else
    {
      //console.log("srNo not matched");
    }
  }

}
/**
setTimeout(function()
{
  //console.log("3 sec timer");
   Socket.send("20001#z#o");
}, 3000);
**/

function logJsonToTable(logData)
{
  var obj = JSON.parse(logData);
  //console.log("new data");
  //console.log(obj.eventID + " " + obj.eff + " " + obj.bkgCPM + " " + obj.areaID + " " + obj.grossCnts + " " + obj.periodSec + " " + obj.remarks);
  var table = document.getElementById("dataTable");
  var row = table.insertRow(1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
  cell1.innerHTML = obj.eventID;
  cell2.innerHTML = obj.eff;
  cell3.innerHTML = obj.bkgCPM;
  cell4.innerHTML = obj.areaID;
  cell5.innerHTML = obj.grossCnts;
  cell6.innerHTML = obj.periodSec;
  cell7.innerHTML = obj.remarks;
}

function updateAreaID(areaIDArg)
{
  areaID = areaIDArg;
  Socket.send("20001#a#a#"+areaID);
  document.getElementById("opID").innerHTML = "AreaID: ";
  document.getElementById("opValue").innerHTML = areaID;
  areaNo = parseInt(areaID/10);
  //console.log(areaNo);
  setAreaNo(areaNo);
  labNo = areaID-(areaNo*10);
  //console.log(labNo);
  setLabNo(labNo);
}
/**
function areaIDToName()
{
  var labName = document.getElementById("lab_0"+labNo).innerHTML;
  //console.log("labName: " + labName);
  var areaDOM = document.getElementById("areaSelect");
  var areaName = areaDOM.options[areaDOM.areaSelect].text;
  //console.log("areaName: " + areaName);
  return areaName + "-" + labName;
}
**/
function changeLabName(areaNoArg)
{
  if(areaNoArg == 0)
  {
    document.getElementById("lab_01").innerHTML = "lab_01";
    document.getElementById("lab_02").innerHTML = "lab_02";
    document.getElementById("lab_03").innerHTML = "lab_03";
    document.getElementById("lab_04").innerHTML = "lab_04";
    document.getElementById("lab_05").innerHTML = "lab_05";
  }
  else if(areaNoArg == 1)
  {
    document.getElementById("lab_01").innerHTML = "lab_11";
    document.getElementById("lab_02").innerHTML = "lab_12";
    document.getElementById("lab_03").innerHTML = "lab_13";
    document.getElementById("lab_04").innerHTML = "lab_14";
    document.getElementById("lab_05").innerHTML = "lab_15";
  }
  else if(areaNoArg == 2)
  {
    document.getElementById("lab_01").innerHTML = "lab_21";
    document.getElementById("lab_02").innerHTML = "lab_22";
    document.getElementById("lab_03").innerHTML = "lab_23";
    document.getElementById("lab_04").innerHTML = "lab_24";
    document.getElementById("lab_05").innerHTML = "lab_25";
  }
  else if(areaNoArg == 3)
  {
    document.getElementById("lab_01").innerHTML = "lab_31";
    document.getElementById("lab_02").innerHTML = "lab_32";
    document.getElementById("lab_03").innerHTML = "lab_33";
    document.getElementById("lab_04").innerHTML = "lab_34";
    document.getElementById("lab_05").innerHTML = "lab_35";
  }
  else if(areaNoArg == 4)
  {
    document.getElementById("lab_01").innerHTML = "lab_41";
    document.getElementById("lab_02").innerHTML = "lab_42";
    document.getElementById("lab_03").innerHTML = "lab_43";
    document.getElementById("lab_04").innerHTML = "lab_44";
    document.getElementById("lab_05").innerHTML = "lab_45";
  }
}

function setAreaNo(areaNoArg)
{
  document.getElementById("areaSelect").selectedIndex = areaNoArg;
  changeLabName(areaNoArg);
}

function getAreaNo(areaNoArg)
{
  areaNo = areaNoArg.value*1;
  areaID = (areaNo*10)+(labNo*1);
  //console.log(areaID);
  Socket.send("20001#a#o#"+areaID);
  changeLabName(areaNo);
}

function setLabNo(labNoArg)
{
  if(labNoArg == 0)
  {
    document.getElementById("lab01").checked = true;
  }
  else if(labNoArg == 1)
  {
    document.getElementById("lab02").checked = true;
  }
  else if(labNoArg == 2)
  {
    document.getElementById("lab03").checked = true;
  }
  else if(labNoArg == 3)
  {
    document.getElementById("lab04").checked = true;
  }
  else if(labNoArg == 4)
  {
    document.getElementById("lab05").checked = true;
  }
}

function getLabNo(labNoArg)
{
  labNo = labNoArg.value;
  areaID = (areaNo*10)+(labNo*1);
  //console.log(areaID);
  Socket.send("20001#a#o#"+areaID);
}

function updatebkgCPM(bkgCPMArg)
{
  bkgCPM = bkgCPMArg;
  Socket.send("20001#b#a#"+bkgCPM);
  document.getElementById("bkgCPM").innerHTML = bkgCPM;
  document.getElementById("opID").innerHTML = "Bkg(cps): ";
  document.getElementById("opValue").innerHTML = bkgCPM;
}

function clickbkgCPM()
{
  var bkgCPMTemp = prompt("Please enter Bkg(cps):", "5");
  var fourNoRegex = /^\d{1,4}$/;
  var result = fourNoRegex.test(bkgCPMTemp);
  if(result)
  {
    bkgCPM = bkgCPMTemp;
    Socket.send("20001#b#o#"+bkgCPM);
	  //console.log(bkgCPM); //print the correct
  }
  else
  {
    alert("Bkg(CPM) value should be between 0 & 9999");
  }
}

function clickDelete()
{
  if (confirm("Delete all log data. Refresh the page to update the table.")) {
    console.log("delete ok");
    Socket.send("20001#d#o#");
  }
}

function updateEff(effArg)
{
  eff = effArg;
  document.getElementById("eff").innerHTML = eff;
  document.getElementById("opID").innerHTML = "Eff: ";
  document.getElementById("opValue").innerHTML = eff;
  Socket.send("20001#e#a#"+eff);
}


function clickEff()
{
  var effTemp = prompt("Please enter Eff(%) bw 1:99:", "25");
  var twoNoRegex = /^\d{1,2}$/;
  var result = twoNoRegex.test(effTemp);
  if(result && (effTemp > 0))
  {
    eff = effTemp;
    Socket.send("20001#e#o#"+eff);
	  //console.log(eff); //print the correct
  }
  else
  {
    alert("Eff(%) value should be between 1 & 99");
  }
}

function setRunMin(runMinArg)
{
  if(runMinArg == 1)
  {
    document.getElementById("1_min").checked = true;
  }
  else if(runMinArg == 5)
  {
    document.getElementById("5_min").checked = true;
  }
  else if(runMinArg == 60000)
  {
    document.getElementById("n_min").checked = true;
  }
  else
  {
    document.getElementById("x_min").checked = true;
    document.getElementById("x").innerHTML = runMinArg;
  }
}

function updateRunMin(runMinArg)
{
  runMin = runMinArg;
  Socket.send("20001#g#a#"+runMin);
  document.getElementById("opID").innerHTML = "Run(min): ";
  document.getElementById("opValue").innerHTML = runMin;
  document.getElementById("x").innerHTML = runMin;
  setRunMin(runMin);
}

function getRunMin(radioRunArg)
{
  //alert(radioRun.value);
  var runMinTemp = 0;
  if(radioRunArg.value == 0)
  {
    runMinTemp = 1;
  }
  else if(radioRunArg.value == 1)
  {
    runMinTemp = 5;
  }
  else if(radioRunArg.value == 2)
  {
    runMinTemp = 60000;
  }
  else if(radioRunArg.value == 3)
  {
    var x = prompt("Please enter Run_Time(min): ", "1");
    if(x != null)
    {
      runMinTemp = x*1;
      //document.getElementById("x").innerHTML = x;
    }
  }

  if (runMinTemp != null)
  {
    runMin = runMinTemp;
    Socket.send("20001#g#o#"+runMin);
	  //console.log(runMin); //print the correct
  }
}

function setUnitSet(unitSetArg)
{
  if(unitSetArg == 0)
  {
    document.getElementById("cnts").checked = true;
    document.getElementById("unitSet").innerHTML = "COUNTS: ";
  }
  else if(unitSetArg == 1)
  {
    document.getElementById("cpm").checked = true;
    document.getElementById("unitSet").innerHTML = "CPM: ";
  }
  else if(unitSetArg == 2)
  {
    document.getElementById("cps").checked = true;
    document.getElementById("unitSet").innerHTML = "CPS: ";
  }
  else if(unitSetArg == 3)
  {
    document.getElementById("bq").checked = true;
    document.getElementById("unitSet").innerHTML = "Bq: ";
  }
}

function updateUnitSet(unitSetArg)
{
  unitSet = unitSetArg;
  Socket.send("20001#h#a#"+unitSet);
  document.getElementById("opID").innerHTML = "Unit: ";
  document.getElementById("opValue").innerHTML = unitSet;
  setUnitSet(unitSet);
}

function getUnitSet(unitSetArg)
{
  unitSet = unitSetArg.value;
  Socket.send("20001#h#o#"+unitSet);
  //console.log(unitSet);
}

function updateCL(clArg)
{
  cl = clArg;
  document.getElementById("cl").innerHTML = cl;
  document.getElementById("opID").innerHTML = "CL(%): ";
  document.getElementById("opValue").innerHTML = cl;
}

function clickCL()
{
  var clTemp = prompt("Please enter CL(%):", "95");
  var twoNoRegex = /^\d{1,2}$/;
  var result = twoNoRegex.test(clTemp);
  if(result && (clTemp > 0))
  {
    cl = clTemp;
    Socket.send("20001#l#o#"+cl);
	  //console.log(cl); //print the correct
  }
  else
  {
    alert("CL(%) value should be between 1 & 99");
  }
}

function clickNext()
{
  Socket.send("20001#n#o");
  //console.log("20001#n#o"); //print the correct
}

function clickStop()
{
  Socket.send("20001#o#o");
  //console.log("20001#o#o"); //print the correct
}

function clickReset()
{
  Socket.send("20001#r#o");
  //console.log("20001#r#o"); //print the correct
}

function clickStart()
{
  Socket.send("20001#s#o");
  //console.log("20001#s#o"); //print the correct
}

function updateBkgMin(bkgMinArg)
{
  bkgMin = bkgMinArg;
  Socket.send("20001#u#a#"+bkgMin);
  document.getElementById("bkgMin").innerHTML = bkgMin;
  document.getElementById("opID").innerHTML = "BkgT(Min): ";
  document.getElementById("opValue").innerHTML = bkgMin;
}

function clickBkgMin()
{
  var bkgMinTemp = prompt("Please enter Bkg(min):", "1");
  var fourNoRegex = /^\d{1,4}$/;
  var result = fourNoRegex.test(bkgMinTemp);
  if (result && (bkgMinTemp > 0))
  {
    bkgMin = bkgMinTemp;
    Socket.send("20001#u#o#"+bkgMin);
	  //console.log(bkgMin); //print the correct
  }
  else
  {
    alert("Bkg(min) value should be between 1 & 9999");
  }
}

function updateSamMin(samMinArg)
{
  samMin = samMinArg;
  Socket.send("20001#v#a#"+samMin);
  document.getElementById("samMin").innerHTML = samMin;
  document.getElementById("opID").innerHTML = "SamT(Min): ";
  document.getElementById("opValue").innerHTML = samMin;
}

function clickSamMin()
{
  var samMinTemp = prompt("Please enter Sam(min):", "1");
  var fourNoRegex = /^\d{1,4}$/;
  var result = fourNoRegex.test(samMinTemp);
  if (result && (samMinTemp > 0))
  {
    samMin = samMinTemp;
    Socket.send("20001#v#o#"+samMin);
	  //console.log(samMin);
  }
  else
  {
    alert("Sam(min) value should be between 1 & 9999");
  }
}

function addRow()
{
  var table = document.getElementById("dataTable");
  var row = table.insertRow(1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);
  var cell7 = row.insertCell(6);
  cell1.innerHTML = eventID;
  cell2.innerHTML = eff;
  cell3.innerHTML = bkgCPM;
  cell4.innerHTML = areaID;
  cell5.innerHTML = grossCnts;
  cell6.innerHTML = periodSec;
  cell7.innerHTML = remarks;
}
